/*
 * Inyección de dependencias de NodeJS
 */
const electron = require('electron');
const {
  app,
  BrowserWindow,
  ipcMain,
  webContents
} = electron;
const request = require('request');
const fs = require('fs');
const http = require('http');
const needle = require('needle');
const feed = require('feed-read');
const os = require('os');
const diskspace = require('diskspace');
const si = require('systeminformation');
const isOnline = require('is-online');
const moment = require('moment-timezone');
const git = require('simple-git/promise');
const {
  exec
} = require('child_process');
var WebSocket = require('ws');
var extract = require('extract-zip');
var shelljsFfi = require("shelljs-ffi");
const pathIma = require('path');

/*
 * Variables Globales
 */
const root_dir = `${__dirname}/`; // Dirección de directorio raíz
var cant_descargas = 0; // Control de cuantos archivos se descargan al tiempo
var wsActive = 0;
var ws = '';
var mainWindow = null; // Variable para la instancia de electron
var inactividad = -1; // Control para ver que se este ejecutando de manera correcta siempre
var tipoAudio = '';
let path = os.platform() === 'win32' ? 'c:' : '/'; // Se verifica que sistema operativo este ejecutando el app
var idSetBx = 0;


var idSetBoxDesactivado = "0";
var licenciaDesactivada = "";

const CMD = {
  Reset: '\x1b[0m',
  Bright: '\x1b[1m',
  Dim: '\x1b[2m',
  Underscore: '\x1b[4m',
  Blink: '\x1b[5m',
  Reverse: '\x1b[7m',
  Hidden: '\x1b[8m',
  FgBlack: '\x1b[30m',
  FgRed: '\x1b[31m',
  FgGreen: '\x1b[32m',
  FgYellow: '\x1b[33m',
  FgBlue: '\x1b[34m',
  FgMagenta: '\x1b[35m',
  FgCyan: '\x1b[36m',
  FgWhite: '\x1b[37m',
  BgBlack: '\x1b[40m',
  BgRed: '\x1b[41m',
  BgGreen: '\x1b[42m',
  BgYellow: '\x1b[43m',
  BgBlue: '\x1b[44m',
  BgMagenta: '\x1b[45m',
  BgCyan: '\x1b[46m',
  BgWhite: '\x1b[47m'
}

function __log(message, ...args) {
  console.log(args.join('%s'), message);
}

/*
 * Manejo de errores previos a la instancia del app
 */
process
  .on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
    console.log('unhandledRejection');
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  })
  .on('uncaughtException', err => {
    console.error(err, 'Uncaught Exception thrown');
    console.log('uncaughtException');
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  });

//Se comienza el proceso de iniciar el app
app.on('ready', () => {

  //Metodo para al cerrar o reiniciar termine todos los procesos hijos
  app.once('window-all-closed', electron.app.quit);
  app.once('before-quit', () => {
    //window.removeAllListeners('close');
  });

  const {
    width,
    height
  } = electron.screen.getPrimaryDisplay().workAreaSize; //Se obtiene el ancho y alto de la pantall ejecutando el app

  //addLog('[INFO] => Iniciando Winbox');

  var player = { //Se inicializa el player desde el primer video en lista
    'block': 0,
    'media': 0
  };
  fs.writeFile(root_dir + "player.json", JSON.stringify(player), 'utf8', function(err) {
    console.log('escribir player');
    if (err) {
      console.log('error escribir player ' + err);
      return console.log(err);
    }
  });

  var inferior = { //Se inicializa el archivo de imagenes inferiores desde la primer imagen
    'actual': 0
  };
  fs.writeFile(root_dir + "inferior.json", JSON.stringify(inferior), 'utf8', function(err) {
    console.log('escribir inferior');
    if (err) {
      console.log('error escribir inferior ' + err);
      return console.log(err);
    }
  });

  var lateral = { //Se inicializa el archivo de imagenes laterales desde la primer imagen
    'actual': 0
  };
  fs.writeFile(root_dir + "lateral.json", JSON.stringify(lateral), 'utf8', function(err) {
    console.log('escribir lateral');
    if (err) {
      console.log('error escribir lateral ' + err);
      return console.log(err);
    }
  });

  if (!fs.existsSync('comunicados.json')) { //Se inicializan los comunicados
    //addLog('[INFO] => No existe archivo de comunicados, se creara uno');
    var comunicados = {
      'comunicados': []
    };
    fs.writeFile(root_dir + "comunicados.json", JSON.stringify(comunicados), 'utf8', function(err) {
      console.log('escribir comunicados');
      if (err) {
        console.log('error escribir comunicados ' + err);
        return console.log(err);
      }
    });
  }

  if (!fs.existsSync(root_dir + 'config.json')) { //Se inicializa la configuracion, de no existir archivo se crea uno vacio
    //addLog('[INFO] => No existe archivo de configuraición, se creara uno');
    var config = {
      'message': 'false'
    };
    fs.writeFile(root_dir + "config.json", JSON.stringify(config), 'utf8', function(err) {
      console.log('escribir config');
      if (err) {
        console.log('error escribir config ' + err);
        return console.log(err);
      }
    });

    getConfig(); //Se llama a la funcion que genera la configuracion del winbox y se ejecuta cada minuto
    setInterval(function() {
      getConfig();
    }, 60000);
  } else {
    getConfig(); //Se llama a la funcion que genera la configuracion del winbox y se ejecuta cada minuto
    setInterval(function() {
      getConfig();
    }, 60000);
  }

  //Se crea la ventana de la aplicacion con las especifaciones de alto y ancho
  mainWindow = new BrowserWindow({
    width: width,
    height: height,
    frame: false,
    fullscreen: true
  });

  mainWindow.loadURL(`file://${__dirname}/app/index.html`); //Se carga el aplicativo


  //Manejo de errores en el aplicativo iniciado
  mainWindow.on('unresponsive', () => {
    //addLog('[INFO] => unresponsive');
    console.log('unresponsive');
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  })
  mainWindow.on('session-end', () => {
    //addLog('[INFO] => session-end');
    console.log('session-end');
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  })
  mainWindow.on('crashed', () => {
    //addLog('[INFO] => crashed');
    console.log('crashed');
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  })
  mainWindow.on('destroyed', () => {
    //addLog('[INFO] => destroyed');
    console.log('destroyed');
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  })

});

//Funcion para descargar los archivos que se envian desde el cms, se descargan los videos, imagenes de canal, imagenes laterales, etc.
function downloadFile(url, nameFile, type) {

  __log(`::: preparando descarga ${url}`, CMD.BgGreen, CMD.Reset);

  if (type == 'media') { //Se verifica el tipo de solicitud, si es un archivo de parrilla o otro tipo de archivo
    var req = request({
      method: 'GET',
      uri: url
    });

    // __log(`::: archivo de tipo multimedia`, CMD.BgGreen, CMD.Reset);

    let tempFile = `${root_dir}app/files/temp/${nameFile}`;
    let compFile = `${root_dir}app/files/completed/${nameFile}`;

    // try {
    //   if (fs.existsSync(tempFile)) {
    //     __log(`::: eliminando archivo temporal ${nameFile}`, CMD.BgYellow, CMD.Reset);
    //     fs.unlinkSync(tempFile);
    //   }
    // } catch (e) {
    //   __log(`::: error eliminando archivo temporal ${nameFile}`, CMD.BgRed, CMD.Reset);
    // }

    var out = fs.createWriteStream(root_dir + 'app/files/temp/' + nameFile); //Se crea un espacio para la descarga

    __log(`::: descargando ${nameFile}`, CMD.BgGreen, CMD.Reset);
    req.pipe(out);

    req.on('end', function() {
      cant_descargas--; //Se resta una descarga del limite para que se pueda descargar una siguiente
      try {

        __log(`::: descarga completa ${nameFile}`, CMD.BgGreen, CMD.Reset);
        __log(`::: verificando ubicacion del archivo`, CMD.BgGreen, CMD.Reset);

        if (!fs.existsSync(root_dir + 'app/files/completed/' + nameFile)) { //Si no existe el archivo se mueve a la carpeta

          __log(`::: realizando transferencia`, CMD.BgGreen, CMD.Reset);

          fs.renameSync(root_dir + 'app/files/temp/' + nameFile, root_dir + 'app/files/completed/' + nameFile);

          __log(`::: transferencia realizada ${nameFile}`, CMD.BgGreen, CMD.Reset);
        }
      } catch (err) {
        __log(`::: error en la descarga \n ${err}`, CMD.BgRed, CMD.Reset);
      }
    });
  }

  if (type != 'media') { //Se verifica el tipo de solicitud, si es un archivo de parrilla o otro tipo de archivo
    console.log('downloadFile tipo NO media');
    var format = url.split('.').pop();
    var name = url.split('/').pop();

    var req = request({
      method: 'GET',
      uri: url
    });

    var out = fs.createWriteStream(root_dir + 'app/files/temp/' + type + '_' + name);
    req.pipe(out);

    req.on('end', function() {
      cant_descargas--;
      setTimeout(function() {
        try {
          if (!fs.existsSync(root_dir + 'app/files/completed/' + type + '_' + name)) {
            fs.renameSync(root_dir + 'app/files/temp/' + type + '_' + name, root_dir + 'app/files/completed/' + type + '_' + name);
            //addLog('[INFO] => Descarga Completada, Archivo: '+type+'_'+name);
            console.log('downloadFile Descarga Completada ' + name);
          }
        } catch (err) {
          console.log('downloadFile error descarga ' + err);
        }


      }, 15000);

    });
  }
  console.log('fin downloadFile');
}

//Funcion que hace el request al cms para obtener la informacion del canal, cliente, contenidos, etc.
function getConfig() {
  console.log('inicio getConfig');
  try { //Se genera dentro de un try and catch para evitar que algun error termine con la ejecucion del aplicativo

    var date = new Date();
    var dateFormated = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    var timeFormated = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    var systemInfo = getSystemInfo(); //Se consulta la informacion del equipo y se guarda en un archivo

    fs.readFile(root_dir + 'config.json', 'utf8', function readFileCallback(err, data) { //Se obtiene el archivo de configuracion para saber que tipo de request se va a hacer

      console.log('lee config');
      if (err) {
        console.log('error lee config ' + err);
      } else {
        var dataLocal = data; //Se guarda una copia de la configuracion existente

        if (data != '') {
          obj = JSON.parse(data); //now it an object
        } else {
          obj = '';
        }

        var host = '';
        var relaunch = 0;

        if (typeof (obj.id) != "undefined") { //El winbox acaba de ser Activado

          //addLog('[INFO] => Winbox Activado');
          idSetBx = obj.id;
          var host = obj.host;
          var data = {
            id: obj.id,
            licencia: obj.license,
            fecha: dateFormated,
            hora: timeFormated
          };
          //WEBSERVICE ENVIAR DIRECCION MAC DANI
          console.log("1");
          fs.readFile(root_dir + "system.json", 'utf8', function readFileCallback(err, dataConfig) {
            objConfig = JSON.parse(dataConfig);
            if (objConfig) {
              if (objConfig.network.mac) {
                needle.post("http://windowschannel.net/winbox/direccionMac", {
                  "mac": objConfig.network.mac,
                  "id": obj.id
                }, null, function(err, resp) {

                });
              }
            }
          });
        } else if (typeof (obj.winbox) != "undefined") { //El winbox ya estaba activado

          var host = obj.host;
          if (typeof (obj.timezone) != "undefined" && obj.timezone != '') {
            var timezone = obj.timezone;
          } else {
            var timezone = 'America/Bogota';
          }
          dateFormated = moment().tz(timezone).format('DD/MM/YYYY');
          timeFormated = moment().tz(timezone).format('HH:mm:ss');
          idSetBx = obj.winbox.id;
          var data = {
            id: obj.winbox.id,
            licencia: obj.winbox.license,
            fecha: dateFormated,
            hora: timeFormated
          };
          //WEBSERVICE ENVIAR DIRECCION MAC DANI
          console.log("2");
          fs.readFile(root_dir + "system.json", 'utf8', function readFileCallback(err, dataConfig) {
            if (dataConfig)
              if (typeof objConfig != "undefined") {
                objConfig = JSON.parse(dataConfig);
              } else {
                var objConfig = JSON.parse(dataConfig);
              }
            if (objConfig) {
              if (objConfig.network.mac) {
                needle.post("http://windowschannel.net/winbox/direccionMac", {
                  "mac": objConfig.network.mac,
                  "id": data.id
                }, null, function(err, resp) {

                });
              }
            }
          });

        } else { //El winbox no ha sido activado
          idSetBx = 0;
          var host = '';
          var data = {
            id: idSetBoxDesactivado,
            licencia: licenciaDesactivada,
            fecha: dateFormated,
            hora: timeFormated
          };
          //WEBSERVICE ESTA DESACTIVADO DANI
          console.log("DESACTIVADO");
          fs.readFile(root_dir + "system.json", 'utf8', function readFileCallback(err, dataConfig) {
            objConfig = JSON.parse(dataConfig);
            if (objConfig) {
              if (objConfig.network.mac) {
                needle.post("http://windowschannel.net/winbox/direccionMac", {
                  "mac": objConfig.network.mac,
                  "offline": 1
                }, null, function(err, resp) {
                  if (resp.body.success) {
                    idSetBoxDesactivado = resp.body.result.id;
                    licenciaDesactivada = resp.body.result.licencia;
                    var content = JSON.stringify({
                      "id": resp.body.result.id,
                      "license": resp.body.result.licencia,
                      "host": "http://windowschannel.net"
                    });
                    fs.writeFile(root_dir + "config.json", content, 'utf8', function(err) {
                      console.log("relaunch AAP");
                      app.relaunch();
                      app.exit(0);
                    });
                    console.log(data);
                  }
                });
              }
            }
          });
        }

        //Aplicativo en pruebas por lo que puede hacer las solicitudes a dos server diferentes ya sea en produccion o desarrollo
        if (host == 'http://angularpage.windowschannel.net') {
          var url_main = 'main';
        } else {
          var url_main = 'winbox';
        }

        needle.post(host + '/' + url_main + '/config', data, null, function(err, resp) { //Request al cms

          if (!err) {

            if (typeof (resp.body.result) != "undefined") {
              var content = JSON.stringify(resp.body.result);
            } else {
              if (typeof (resp.body.success) != "undefined") {
                var content = JSON.stringify(resp.body);
              } else {
                var content = dataLocal;
              }
            }

            //Se verifica que la configuracion sea diferente a la que ya existe
            if (dataLocal != JSON.stringify(resp.body.result)) {
              addLog('[INFO] => Actualizando Configuración');
            }

            //envia al log la nueva configuracion
            var contConfig = JSON.parse(content);
            /* if(typeof(contConfig.channel.blocks) == undefined || typeof(contConfig.channel.blocks) == 'undefined') {
                 enviaLog(contConfig.winbox.id, content);
             }
*/
            //Se guarda la nueva configuracion o se guarda la misma que existia
            fs.writeFile(root_dir + "config.json", content, 'utf8', function(err) {
              console.log('escribe config');
              if (err) {
                return console.log(err);
              }
              fs.readFile(root_dir + 'config.json', 'utf8', function readFileCallback(err, data) {
                console.log('lee config para escribir');
                if (err) {
                  console.log('error lee config para escribir ' + err);
                  console.log(err);
                } else {
                  try {
                    if (mainWindow != null) { //Si la funcion de config se esta ejecutando por primera vez no se verifica inactividad
                      if (inactividad != -1) {
                        inactividad = 0;
                      }

                      mainWindow.webContents.send('prueba_date'); //Se envia un ping a el main render para verificar si se mantiene activo
                      //setTimeout(function(){  checkInactividad();  }, 5000);//Despues de cinco segundos se verifica si hubo respuesta
                    }

                    obj = JSON.parse(data); //now it an object
                    idSetBx = obj.winbox.id;
                    //Conexion al WebSocket, cada minuto se desconecta y vuelve a conectarse para evitar desconexiones
                    if (wsActive == 0) {
                      ws = new WebSocket('ws://websocket.windowschannel.net/' + obj.winbox.id);
                      ws.on('close', function() {
                        wsActive = 0;
                        console.log('close ws');
                      });
                      ws.on('error', function() {
                        wsActive = 0;
                        console.log('error ws');
                      });
                      ws.on('open', function() {
                        console.log('open ws');
                        wsActive = 1;
                      });
                      ws.on('message', function(message) {
                        obj = JSON.parse(message);
                        mainWindow.webContents.send(obj.type, obj);
                      });
                    } else {
                      ws.close();
                      ws = new WebSocket('ws://websocket.windowschannel.net/' + obj.winbox.id);
                      ws.on('close', function() {
                        wsActive = 0;
                        console.log('close ws');
                      });
                      ws.on('error', function() {
                        wsActive = 0;
                        console.log('error ws');
                      });
                      ws.on('open', function() {
                        console.log('open ws');
                        wsActive = 1;
                      });
                      ws.on('message', function(message) {
                        obj = JSON.parse(message);
                        mainWindow.webContents.send(obj.type, obj);
                      });
                    }
                    //Funcion que si del cms se envia ancho y alto se ejecuta el script, siempre y cuando sea linux y las dimensiones sean diferentes a las actuales
                    if (obj.winbox.alto != 0 && obj.winbox.alto != 0) {
                      console.log('antes de modificar pantalla que esta comentado');
                      const {
                        width,
                        height
                      } = electron.screen.getPrimaryDisplay().workAreaSize;
                      if (obj.winbox.ancho != width) {
                        if (os.platform() == 'linux' || os.platform() == 'Linux') {
                          var screen = 'gtf ' + obj.winbox.ancho + ' ' + obj.winbox.alto + ' 60 | grep "Modeline .*" | sed -e "s/  Modeline //g" | xargs xrandr --newmode; xrandr | grep ".* connected" | sed -e "s/ connected.*//g" | tr -d "\n" | xargs -0 -I name xrandr --addmode name ' + obj.winbox.ancho + 'x' + obj.winbox.alto + '_60.00; xrandr | grep ".* connected" | sed -e "s/ connected.*//g" | tr -d "\n" | xargs -0 -I name xrandr --output name --mode ' + obj.winbox.ancho + 'x' + obj.winbox.alto + '_60.00;';
                          fs.writeFile(root_dir + "screen.sh", screen, 'utf8', function(err) {
                            console.log('escribe screen');
                            if (err) {
                              console.log('error escribe screen ' + err);
                              return console.log(err);
                            }
                            fs.chmod(root_dir + "screen.sh", 0777, function(err) {
                              exec('~/projectwindowschannel/screen.sh', function(error, stdout, stderr) {
                                console.log('Cambio Resolucion Pantalla');
                                addLog('[INFO] => Reiniciando Aplicación');
                                //app.relaunch();
                                avisaApagadoComandos(idSetBx);
                                app.exit(0);
                              })
                            })
                          });
                        }
                      }
                    }

                    //Se empieza la descarga de contenidos asignados al winbox
                    if (typeof (obj.host) != "undefined") {
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.blocks) != "undefined") {
                        obj.channel.blocks.forEach(function(block, index) {
                          if (typeof (block.media) != "undefined") {
                            block.media.forEach(function(media, index) {
                              if (media.type != 'streaming' && media.type != 'text') { //Se verifica el tipo de contenido
                                if ((typeof (media.value_new) == undefined || media.value_new == null) || (media.value_new == 0)) { //Se verifica que si el archivo esta en el nuevo servidor o el viejo
                                  if (!fs.existsSync(root_dir + 'app/files/completed/' + media.id + '-' + media.value.split('/')[3]) && cant_descargas < 4) { //Se verifica que el archivo no haya sido ya descargado y el limite de archivos en descarga
                                    cant_descargas++;
                                    downloadFile('http://windowschannel.net' + media.value, media.id + '-' + media.value.split('/')[3], 'media');
                                  }
                                } else {
                                  if (!fs.existsSync(root_dir + 'app/files/completed/' + media.value_new.split('/')[7]) && cant_descargas < 4) {
                                    cant_descargas++;
                                    downloadFile(obj.host + media.value_new, media.value_new.split('/')[7], 'media');
                                  }
                                }
                              }
                            });
                          }
                        });
                      }

                      if (typeof (obj.winbox.update) != "undefined") { //Se verifica que no se haya solicitado una actualización
                        var updWin = upgradeWinbox(obj.host, obj.winbox.license);
                      }
                      if (typeof (obj.winbox.media) != "undefined") { //Se verifica que no se haya solicitado un borrado de media
                        var updWin = removeMediaFiles();
                      }
                      if (typeof (obj.winbox.refresh) != "undefined") { //Se verifica que no se haya solicitado un reinicio del app
                        //addLog('[INFO] => Reiniciando Winbox');
                        mainWindow = null;
                        app.quit();
                      }

                                                  if(typeof(obj.winbox.reboot) != "undefined") {//Se verifica que no se haya solicitado un reinicio del equipo
                                                      //addLog('[INFO] => Reiniciando Equipo');
                                                      //shelljsFfi.exec('reboot');
                                                      ejecutaComandoTerminal('reboot');

                                                  }/*
                                                  if(typeof(obj.winbox.salida_audio) != "undefined" && obj.winbox.salida_audio == 'DIGITAL' && tipoAudio != obj.winbox.salida_audio) {//Se modifica la salida de audio para digital
                                                      console.log('he cambiado a digital');
                                                      tipoAudio = obj.winbox.salida_audio;
                                                      //shelljsFfi.exec('pacmd set-card-profile 0 output:hdmi-stereo');
                                                      ejecutaComandoTerminal('pacmd set-card-profile 0 output:hdmi-stereo');

                                                  }
                                                  if(typeof(obj.winbox.salida_audio) != "undefined" && obj.winbox.salida_audio == 'ANALOGICO' && tipoAudio != obj.winbox.salida_audio) {//Se modifica la salida de audio para analogico
                                                      console.log('Cambiando sonido a analogico');
                                                      tipoAudio = obj.winbox.salida_audio;
                                                      //shelljsFfi.exec('pacmd set-card-profile 0 output:analog-stereo');
                                                      ejecutaComandoTerminal('pacmd set-card-profile 0 output:analog-stereo');

                                                  }
                                                  if(typeof(obj.winbox.screen_signal) != "undefined" && obj.winbox.screen_signal != '') {//
                                                      console.log('Cambiando Resolucion de Pantalla pasa a '+obj.winbox.screen_signal);
                                                      shelljsFfi.exec('export DISPLAY=:0 && xset dpms force '+obj.winbox.screen_signal);
                                                      //ejecutaComandoTerminal('export DISPLAY=:0 && xset dpms force '+obj.winbox.screen_signal);

                                                  }*/
                      if (typeof (obj.winbox.getScreenshot) != "undefined" && obj.winbox.getScreenshot == 'S') { //
                        console.log('Haciendo una screenshot');
                        shelljsFfi.exec('gnome-screenshot -f ' + obj.winbox.id + '.jpg');
                        //ejecutaComandoTerminal('gnome-screenshot -f '+obj.winbox.id+'.jpg');
                        console.log('Llamando a enviaIMG');
                        enviaIMG(obj.winbox.id, obj.winbox.id + '.jpg');
                      }

                      var upd = updateBiblioteca(obj.host, obj.winbox.id); //Se actualiza la biblioteca del winbox en linea

                      //Se descargan los comunicados que esten disponibles
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.comunicados) != "undefined" && obj.channel.comunicados.length > 0) {
                        fs.readFile(root_dir + 'comunicados.json', 'utf8', function readFileCallback(err, data) {
                          if (err) {
                            console.log(err);
                          }
                          objC = JSON.parse(data);

                          var comunicados = [];
                          var comunicadosJSON = [];
                          objC.comunicados.forEach(function(comunicado, index) {
                            if (comunicado.reproducido == 'no') {
                              comunicados.push(comunicado.id);
                              comunicadosJSON.push(comunicado);
                            }

                          });

                          //Se crea el archivo de los comunicados, donde se determina cuales ya se han reproducido
                          obj.channel.comunicados.forEach(function(comunicado, index) {
                            if (comunicados.indexOf(comunicado.id) == '-1') {
                              comunicado.reproducido = 'no';
                              comunicadosJSON.push(comunicado);
                              if ((typeof (comunicado.value_new) == undefined || comunicado.value_new == null) || (comunicado.value_new == -1) || (comunicado.value_new == 0)) {
                                var url = comunicado.value;
                                var url_name = 'comunicado_' + comunicado.id + '_' + comunicado.value.split('/').pop();
                              } else {
                                var url = comunicado.value_new;
                                var url_name = 'comunicado_' + comunicado.id + '_' + comunicado.value.split('/').pop();
                              }
                              if (comunicado.type != 'streaming' && comunicado.type != 'text') {
                                if (!fs.existsSync(root_dir + 'app/files/completed/' + url)) {
                                  downloadFile(obj.host + url, url_name, 'media');
                                }
                              }
                            }
                          });

                          var comunicadosJSON = {
                            "comunicados": comunicadosJSON
                          };
                          fs.writeFile(root_dir + "comunicados.json", JSON.stringify(comunicadosJSON), 'utf8', function(err) {
                            if (err) {
                              return console.log(err);
                            }
                          });

                        });
                      } else {
                        //De no haber nuevos comunicados igualmente se abre y guarda el archivo de comunicados con los reproducidos y los por reproducir
                        fs.readFile(root_dir + 'comunicados.json', 'utf8', function readFileCallback(err, data) {

                          if (err) {
                            console.log(err);
                          }
                          if (data != '') {
                            objC = JSON.parse(data);

                            var comunicados = [];
                            var comunicadosJSON = [];
                            objC.comunicados.forEach(function(comunicado, index) {
                              if (comunicado.reproducido == 'no') {
                                comunicados.push(comunicado.id);
                                comunicadosJSON.push(comunicado);
                              }

                            });
                          } else {
                            var comunicados = [];
                            var comunicadosJSON = [];
                          }

                          var comunicadosJSON = {
                            "comunicados": comunicadosJSON
                          };
                          fs.writeFile(root_dir + "comunicados.json", JSON.stringify(comunicadosJSON), 'utf8', function(err) {
                            if (err) {
                              return console.log(err);
                            }
                          });
                        });
                      }

                      //Se consultan y descargan las noticas ya sean por rss o noticias propias
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.rss2) != "undefined" && typeof (obj.channel.rss2[0]) != "undefined") {

                        if (obj.channel.rss2[0].dir == 'vertical') { //Si son noticias verticales se incluyen saltos de linea, sino se colocan espacios
                          var espacio = '<br>';
                        } else {
                          var espacio = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        }

                        //Se hace Solictud para rss con url externa
                        if (typeof (obj.channel.rss2) != "undefined" && typeof (obj.channel.rss2[0]) != "undefined" && obj.channel.rss2[0].enabled == true) {
                          //console.log("1 rss2 "+obj.channel.rss2[0].value);
                          feed(obj.channel.rss2[0].value, function(err, articles) { //Solictud a la direccion por las rss
                            if (err) {
                              //console.log("1 Error rss2--> "+err);
                              var newsObject = {
                                'news': ''
                              };
                              fs.writeFile(root_dir + "news2.json", JSON.stringify(newsObject), 'utf8', function(err) {
                                if (err) {
                                  return console.log(err);
                                }
                              });
                            } else {
                              var news = '';
                              //console.log("1 rss articulos "+JSON.stringify(articles));
                              articles.forEach(function(article, index) {
                                if (news == '') {
                                  news = article.title;
                                } else {
                                  news = news + espacio + article.title;
                                }
                              });
                              var newsObject = {
                                'newsPropias': false,
                                'news': news.replace(/<br\s*\/?>/mg, "\n")
                              };
                              fs.writeFile(root_dir + "news2.json", JSON.stringify(newsObject), 'utf8', function(err) {
                                if (err) {
                                  return console.log(err);
                                }
                              });
                            }
                          });
                        } else {
                          var newsObject = {
                            'newsPropias': false,
                            'news': ''
                          };
                          fs.writeFile(root_dir + "news2.json", JSON.stringify(newsObject), 'utf8', function(err) {
                            if (err) {
                              return console.log(err);
                            }
                          });

                        }

                      } else {
                        var newsObject = {
                          'newsPropias': false,
                          'news': ''
                        };
                        fs.writeFile(root_dir + "news2.json", JSON.stringify(newsObject), 'utf8', function(err) {
                          if (err) {
                            return console.log(err);
                          }
                        });

                      }
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.rss) != "undefined" && typeof (obj.channel.rss[0]) != "undefined" && obj.channel.rss[0].value != "") {

                        if (obj.channel.rss[0].dir == 'vertical') {
                          var espacio = '<br>';
                        } else {
                          var espacio = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                        if (obj.host == 'http://angularpage.windowschannel.net') {
                          var main = "/main/noticias/";
                        } else {
                          var main = "/noticias.xml?canal=";
                        }

                        //Se hace solicitud para noticias por servidores propios
                        //console.log("2 noticias propias "+obj.host + main + obj.channel.id);
                        feed(obj.host + main + obj.channel.id, function(err, articles) {
                          try {

                            if (err || (articles.length == 0 && (typeof (obj.channel.rss) != "undefined") && obj.channel.rss != null && typeof (obj.channel.rss[0].value) != "undefined" && obj.channel.rss[0].value != "" && obj.channel.rss[0].enabled == true)) {
                              //console.log("2 Error noticias--> "+err);
                              //console.log("3 rss "+obj.channel.rss[0].value);
                              feed(obj.channel.rss[0].value, function(err, articles) {
                                if (err) {
                                  //console.log("3 Error rss--> "+err);
                                  var newsObject = {
                                    'news': ''
                                  };
                                  fs.writeFile(root_dir + "news.json", JSON.stringify(newsObject), 'utf8', function(err) {
                                    if (err) {
                                      return console.log(err);
                                    }
                                  });
                                } else {
                                  var news = '';
                                  //console.log("3 rss articulos "+JSON.stringify(articles));
                                  articles.forEach(function(article, index) {
                                    if (news == '') {
                                      news = article.title;
                                    } else {
                                      news = news + espacio + article.title;
                                    }
                                  });
                                  var newsObject = {
                                    'newsPropias': false,
                                    'news': news.replace(/<br\s*\/?>/mg, "\n")
                                  };
                                  fs.writeFile(root_dir + "news.json", JSON.stringify(newsObject), 'utf8', function(err) {
                                    if (err) {
                                      return console.log(err);
                                    }
                                  });
                                }
                              });
                            } else {
                              var news = '';
                              //console.log("2 noticias articulos "+JSON.stringify(articles));
                              articles.forEach(function(article, index) {
                                if (news == '') {
                                  if (article.title != '' && article.content == '') {
                                    news = article.title.toUpperCase();
                                  }
                                  if (article.title == '' && article.content != '') {
                                    news = article.content;
                                  }
                                  if (article.title != '' && article.content != '') {
                                    news = article.title.toUpperCase() + ': ' + article.content;
                                  }

                                } else {
                                  if (article.title != '' && article.content == '') {
                                    news = news + espacio + article.title.toUpperCase();
                                  }
                                  if (article.title == '' && article.content != '') {
                                    news = news + espacio + article.content;
                                  }
                                  if (article.title != '' && article.content != '') {
                                    news = news + espacio + article.title.toUpperCase() + ': ' + article.content;
                                  }
                                }
                              });
                              var newsObject = {
                                'newsPropias': true,
                                'news': news.replace(/<br\s*\/?>/mg, "\n")
                              };
                              fs.writeFile(root_dir + "news.json", JSON.stringify(newsObject), 'utf8', function(err) {
                                if (err) {
                                  return console.log(err);
                                }
                              });
                            }
                          } catch (err) {
                            console.log(err);
                          }

                        });

                      }

                      //Se verifica si se envia logo de canal y se descarga
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.logo) != "undefined") {
                        if (obj.channel.logo.type == 'image') {
                          if (obj.channel.logo.value_new == '0' || obj.channel.logo.value_new == 0) {
                            var url = obj.channel.logo.value;
                            if (!fs.existsSync(root_dir + 'app/files/completed/logo_channel_' + url.split('/').pop())) {
                              downloadFile(obj.host + url, 1, 'logo_channel');
                            }
                          } else {
                            var url = obj.channel.logo.value_new;
                            if (!fs.existsSync(root_dir + 'app/files/completed/logo_channel_' + url.split('/').pop())) {
                              downloadFile(obj.host + url, 1, 'logo_channel');
                            }
                          }
                        }
                      }

                      //Se verifica si se envio imagen de decoracion de canal y de haberla se descarga
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.background) != "undefined") {
                        if (obj.channel.background.enabled == true) {
                          if (obj.channel.background.value_new == 0 || obj.channel.background.value_new == '0') {
                            var url = obj.channel.background.value;
                            if (!fs.existsSync(root_dir + 'app/files/completed/background_channel_' + url.split('/').pop())) {
                              downloadFile(obj.host + url, 1, 'background_channel');
                            }
                          } else {
                            var url = obj.channel.background.value_new;
                            if (!fs.existsSync(root_dir + 'app/files/completed/background_channel_' + url.split('/').pop())) {
                              downloadFile(obj.host + url, 1, 'background_channel');
                            }
                          }
                        }
                      }

                      //Se verifica si se enviaron imagenes laterales y estas se descargan
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.lateral) != "undefined") {
                        if (obj.channel.lateral.enabled == true) {
                          obj.channel.imagenesLaterales.forEach(elem => {
                            if (elem.picture_new == 0 || elem.picture_new == '0') {
                              var url = elem.picture;
                              if (!fs.existsSync(root_dir + 'app/files/completed/lateral_image_' + url.split('/').pop())) {
                                downloadFile(obj.host + url, 1, 'lateral_image');
                              }
                            } else {
                              var url = elem.picture_new;
                              if (!fs.existsSync(root_dir + 'app/files/completed/lateral_image_' + url.split('/').pop())) {
                                downloadFile(obj.host + url, 1, 'lateral_image');
                              }
                            }
                          });
                        }
                      }

                      //Se verifica si se enviaron imagenes inferiores y estas se descargan
                      if (typeof (obj.channel) != "undefined" && typeof (obj.channel.lateral) != "undefined") {
                        if (obj.channel.lateral.bottom_enabled == true) {
                          obj.channel.imagenesInferiores.forEach(elem => {
                            if (elem.picture_new == '0' || elem.picture_new == 0) {
                              var url = elem.picture;
                              if (!fs.existsSync(root_dir + 'app/files/completed/bottom_image_' + url.split('/').pop())) {
                                downloadFile('http://windowschannel.net' + url, 1, 'bottom_image');
                              }
                            } else {
                              var url = elem.picture_new;
                              if (!fs.existsSync(root_dir + 'app/files/completed/bottom_image_' + url.split('/').pop())) {
                                downloadFile(obj.host + url, 1, 'bottom_image');
                              }
                            }
                          });
                        }
                      }

                      //Se verifica si se envio imagen de logo de cliente y de haberla se descarga
                      if (typeof (obj.client_logo) != "undefined") {
                        if (obj.client_logo.type == 'image') {
                          if (obj.client_logo.value_new == 0 || obj.client_logo.value_new == '0') {
                            var url = obj.client_logo.value;
                            if (!fs.existsSync(root_dir + 'app/files/completed/client_logo_' + url.split('/').pop())) {
                              downloadFile(obj.host + url, 1, 'client_logo');
                            }
                          } else {
                            var url = obj.client_logo.value_new;
                            if (!fs.existsSync(root_dir + 'app/files/completed/client_logo_' + url.split('/').pop())) {
                              downloadFile(obj.host + url, 1, 'client_logo');
                            }
                          }
                        }
                      }

                    }
                  } catch (err) {
                    console.log(err);
                  }


                }
              });
            });
          }

          //Si se define un reinicio este se ejecuta
          if (relaunch == 1) {
            //addLog('[INFO] => Reiniciando Aplicación');
            //app.relaunch()
            avisaApagadoComandos(idSetBx);
            app.exit(0)
          }
        });

      }
    });
  } catch (err) {
    console.log(err);
  } finally {
    console.log("entering and leaving the finally block");
  }

  console.log('fin getConfig');
}


//Funcion que consulta toda la informacion del equipo y la guarda en un archivo de configuracion del sistema
function getSystemInfo() {
  var systemInfo = {};
  diskspace.check(path, function(err, result) {
    isOnline().then(online => { //Primero se verifica que exista una conexion a internet
      systemInfo.online = online;
      si.networkInterfaceDefault(function(data) { //Se obtiene la informacion de la red (ip, mascara, etc)
        var currentInterface = data;
        var coloso = 0;
        si.networkInterfaces(function(data) {
          data.forEach(elem => {
            if (elem.iface == currentInterface) {
              systemInfo.network = {
                "ip": elem.ip4,
                "mac": elem.mac
              };
              coloso = 1;
            }

          });
          if (coloso == 0) {
            systemInfo.network = {
              "ip": "Sin Conexión",
              "mac": "Sin Conexión"
            };
          }
          //Se obtiene la informacion del disco
          systemInfo.disk = {
            "diskFreeAmount": (result.free / 1073741824).toFixed(2),
            "diskTotalAmount": (result.total / 1073741824).toFixed(2),
            "diskUsedPer": 100 - (((result.free / 1073741824).toFixed(2) * 100) / ((result.total / 1073741824).toFixed(2))).toFixed(2),
            "diskFreePer": (((result.free / 1073741824).toFixed(2) * 100) / ((result.total / 1073741824).toFixed(2))).toFixed(2)
          };
          sendDiskInfo(result.total, result.free);

          //Se obtiene la informacion de la memoria ram
          systemInfo.ram = {
            "ramFreeAmount": (os.freemem() / 1073741824).toFixed(2),
            "ramTotalAmount": (os.totalmem() / 1073741824).toFixed(2),
            "ramUsedPer": 100 - (((os.freemem() / 1073741824).toFixed(2) * 100) / ((os.totalmem() / 1073741824).toFixed(2))).toFixed(2),
            "ramFreePer": (((os.freemem() / 1073741824).toFixed(2) * 100) / ((os.totalmem() / 1073741824).toFixed(2))).toFixed(2)
          };

          //Se escribe la informacionen un archivo
          fs.writeFile(root_dir + "system.json", JSON.stringify(systemInfo), 'utf8', function(err) {
            if (err) {
              return console.log(err);
            }
          });
          return systemInfo;
        });
      });
    });
  });
}

//Funcion para indicar al cms cuanto de espacio en disco esta siendo ocupado y cuanto es el total, esta informacion se obtiene al crear los datos del sistema
function sendDiskInfo(total, free) {
  fs.readFile(root_dir + 'config.json', 'utf8', function readFileCallback(err, data) {
    if (err) {
      //console.log(err);
    }
    try {
      obj = JSON.parse(data); //now it an object
      if (typeof (obj.winbox) != undefined && obj.winbox != null && obj.winbox != '') {
        var data = {
          id: obj.winbox.id,
          licencia: obj.winbox.license,
          total_disco: (total / 1048576).toFixed(0),
          espacio_disco: (free / 1048576).toFixed(0)
        };

        if (obj.host == 'http://angularpage.windowschannel.net') {
          var url_main = 'main';
        } else {
          var url_main = 'winbox';
        }
        needle.post(obj.host + '/' + url_main + '/espacio_disco', data, null, function(err, resp) {
          //console.log(resp);
          return resp;
        });
      } else {
        return 0;
      }

    } catch (err) {
      console.log(err);
    }


  });
}

//Se envia al cms la informacion de los archivos que se encuentran descargados en el equipo
function updateBiblioteca(host, winbox) {
  var list = [];
  var list_string = '';
  fs.readdir(root_dir + 'app/files/completed/', (err, files) => { //Se lee el directorio para conseguir todos lo archivos
    files.forEach(file => {
      if (file != '.gitignore') { //Se ignora el archivo de git
        if (file.split('.').pop().toUpperCase() == 'MP4') {
          if (list_string == '') {
            list_string = file;
            list.push(file);
          } else {
            list_string = list_string + ';' + file;
            list.push(file);
          }
        }
      }

    });
    var data = {
      winbox: winbox,
      videos: list_string
    };

    if (host == 'http://angularpage.windowschannel.net') {
      var url_main = 'main';
    } else {
      var url_main = 'winbox';
    }
    needle.post(host + '/' + url_main + '/biblioteca_winbox', data, null, function(err, resp) {
      //console.log(resp);
      return list;
    });
  })
}

//Funcion que realiza un pull al repositorio y actualiza los archivos en disco
function upgradeWinbox(host, license) {
  var data = {
    licencia: license
    //,version:? se debe agregar la version para que se sepa que version tiene
  };
  if (host == 'http://angularpage.windowschannel.net') {
    var url_main = 'main';
  } else {
    var url_main = 'winbox';
  }

  needle.post(host + '/' + url_main + '/upgrade', data, null, function(err, resp) {
    //console.log(resp);
    //addLog('[INFO] => RESP '+JSON.stringify(resp.body));
    var url = host + '/files/707-' + resp.body.id + '-installer_file/' + resp.body.file;
    //addLog('[INFO] => Actualizando Equipo '+url);
    downloadAndUnZIP(url, resp.body.file);
  })


}

//Funcion que obtiene el listado de archivos descargados y los elimina para que sean descargados nuevamente
function removeMediaFiles() {
  //addLog('[INFO] => Borrando Media Data');
  fs.readdir(root_dir + 'app/files/completed/', (err, files) => {
    files.forEach(file => {
      if (file != '.gitignore' && file != 'readme.txt') {
        fs.unlinkSync(root_dir + 'app/files/completed/' + file);
      }
    });
  })
}

//Funcion para agregar una nueva entrada al LOG
function addLog(mensaje) {
  var date = new Date();
  var dateFormated = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  var timeFormated = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
  mensaje = '[ ' + dateFormated + ' ' + timeFormated + ' ] ' + mensaje;
  if (fs.existsSync(root_dir + 'app/logs/log_' + dateFormated + '.log')) {
    fs.appendFile(root_dir + 'app/logs/log_' + dateFormated + '.log', '\n' + mensaje, function(err) {
      if (err) {
        console.log(err);
      }
    });
  } else {
    fs.writeFile(root_dir + 'app/logs/log_' + dateFormated + '.log', mensaje, 'utf8', function(err) {
      if (err) {
        return console.log(err);
      }
    });
  }
}

//Funcion que verifica que el main render ha dado una respuesta y no esta inactivo
function checkInactividad() {
  if (inactividad == 0) { //De estar inactivo se reinicia
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  }
}

//Funcion para descargar los archivos que se envian desde el cms, se descargan los videos, imagenes de canal, imagenes laterales, etc.
function downloadAndUnZIP(url, nameFile) {
  //addLog('[INFO] => downloadAndUnZIP');
  var file = fs.createWriteStream(root_dir + "app/files/temp/" + nameFile);
  var request = http.get(url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close();
      extract(root_dir + 'app/files/temp/' + nameFile, {
        dir: root_dir
      }, function(err) {
        if (err) {
          //addLog('[INFO] => Error Descomprimir ZIP: '+err);
        } else {
          //addLog('[INFO] => Descomprimir ZIP Completado, 2 Archivo: '+nameFile);
          fs.unlink(root_dir + 'app/files/temp/' + nameFile, (err) => {
            if (err) {
              //addLog('ERROR al eliminar '+err);
            } else {
              //addLog('DELETED FILE '+root_dir+'app/files/zip/'+nameFile);
              ejecutaComandoTerminal('reboot');
              //app.quit();
            }
          });
        }
      });
    });
  });
}

//Funcion para enviar LOG al CMS
function enviaLog(idSetBox, mensaje) {
  //addLog("enviaLog ");
  //enviaIMG(idSetBox);
  var datos = {
    idSetbox: idSetBox,
    l: mensaje
  };
  needle.post("http://windowschannel.net/wsReportes.logs", datos, null, function(err, resp) {
    //addLog("enviaLog resp webservice "+err+" "+resp);
  })
}

//Funcion para enviar capturas de pantalla al CMS
function enviaIMG(idSetBox, imagen) {

  //prepara a la app para leer

  //lee la imagen
  fs.readFile(imagen, (err, data) => {

    //error handle
    if (err) {
      if (typeof variable !== 'undefined') {
        // the variable is defined
        addLog("enviaIMG resp crear imagen " + err + " " + resp);
      } else {
        addLog("enviaIMG resp crear imagen " + err);
      }
    }

    //get image file extension name
    let extensionName = pathIma.extname('/home/winbox/projectwindowschannel/' + imagen);

    //convert image file to base64-encoded string
    let base64Image = new Buffer(data, 'binary').toString('base64');
    var datos = {
      idSetbox: idSetBox,
      captura: base64Image
    };
    console.log("enviaIMG img" + imagen);
    needle.post("http://windowschannel.net/wsReportes.capturasPantalla", datos, null, function(err, resp) { });

  });

}

function ejecutaComandoTerminal(comando) {
  shelljsFfi.exec(comando, {
    async: true,
    silent: true
  });

  __log('REINICIAR', CMD.BgWhite, CMD.FgBlack, CMD.Reset);

}

function avisaApagadoComandos(idWin) {
  mainWindow = null;
  ws2 = new WebSocket('ws://websocket.windowschannel.net/' + idWin);
  ws2.on('close', function() {
    console.log('close ws');
  });
  ws2.on('error', function() {
    console.log('error ws');
  });
  ws2.on('open', function() {
    console.log('open ws');
    var datosEnvio = {
      "type": "cerrarApp",
      "msg": "",
      "id": idWin,
      "duration": "",
      "color": ""
    };
    ws2.send(JSON.stringify(datosEnvio));
    ws2.close();
  });
}

//Funcion llamada desde el main render que indica que todavia esta activo

ipcMain.on('prueba_date_devuelta', (event, arg) => {
  //inactividad = 1;
  event.returnValue = 2;
  if (mainWindow) {
    mainWindow.webContents.send('response', 2);
  }
})

//Funcion que ejecuta los comandos enviados desde el main render
ipcMain.on('async', (event, arg) => {

  if (arg == 3) { //Funcion para quitar la licencia
    fs.readFile(root_dir + 'config.json', 'utf8', function readFileCallback(err, data) {
      if (err) {
        //console.log(err);
        event.returnValue = 2;
        mainWindow.webContents.send('response', 2);
      }
      obj = JSON.parse(data); //now it an object
      idSetBx = obj.winbox.id;
      var data = {
        id: obj.winbox.id,
        licencia: obj.winbox.license,
      };

      if (obj.host == 'http://angularpage.windowschannel.net') {
        var url_main = 'main';
      } else {
        var url_main = 'winbox';
      }
      needle.post(obj.host + '/' + url_main + '/desactivar', data, null, function(err, resp) {

        //addLog('[INFO] => Borrando Licencia');
        var config = {
          'message': 'false'
        };
        fs.writeFile(root_dir + "config.json", JSON.stringify(config), 'utf8', function(err) {
          if (err) {
            event.returnValue = 2;
            mainWindow.webContents.send('response', 2);
            return console.log(err);
          }
          fs.readdir(root_dir + 'app/files/completed/', (err, files) => {
            files.forEach(file => {
              if (file != '.gitignore' && file != 'readme.txt') {
                fs.unlinkSync(root_dir + 'app/files/completed/' + file);
              }
            });

            //app.relaunch()
            avisaApagadoComandos(idSetBx);
            app.exit(0)
            event.returnValue = 1;
          })

        });
      });

    });

  }
  if (arg == 2) { //Funcion para borrar los archivos multimedia
    //addLog('[INFO] => Borrando Media Data');
    fs.readdir(root_dir + 'app/files/completed/', (err, files) => {
      files.forEach(file => {
        if (file != '.gitignore' && file != 'readme.txt') {
          fs.unlinkSync(root_dir + 'app/files/completed/' + file);
        }

      });
      event.returnValue = 1;
      mainWindow.webContents.send('response', 1);
    })
  }

  if (arg == 1) { //Funcion para reiniciar el aplicativo
    event.returnValue = 1;
    mainWindow.webContents.send('response', 1);
    //addLog('[INFO] => Reiniciando Aplicación');
    //app.relaunch()
    avisaApagadoComandos(idSetBx);
    app.exit(0)
  }

  if (arg == 4) { //Funcion para forzar una actualizacion
    fs.readFile(root_dir + 'config.json', 'utf8', function readFileCallback(err, data) {
      if (err) {
        //console.log(err);
        event.returnValue = 2;
        mainWindow.webContents.send('response', 2);
        return;
      }
      obj = JSON.parse(data); //now it an object
      var data = {
        licencia: obj.winbox.license
      };

      // if (obj.host == 'http://angularpage.windowschannel.net') {
      //   var url_main = 'main';
      // } else {
      //   var url_main = 'winbox';
      // }

      needle.post('http://windowschannel.net/winbox/upgrade', data, null, function(err, resp) {
        var url = 'http://windowschannel.net/files/707-' + resp.body.id + '-installer_file/' + resp.body.file;
        //addLog('[INFO] => Actualizando Equipo '+url);
        downloadAndUnZIP(url, resp.body.file);
      })


    });
  }
});

//Funcion desde el main render para verificar que contenido se esta reproduciendo y guardar el proximo a reproducir
ipcMain.on('getCurrentBlock', (event, arg) => {
  fs.readFile(root_dir + 'player.json', 'utf8', function readFileCallback(err, data) {
    if (err) {
      console.log(err);
    } else {
      check = JSON.parse(data);
      if (data == '' || data == null || check == null) {
        obj = JSON.parse('{"block":0,"media":0}');
      } else {
        obj = JSON.parse(data);
      }
      event.returnValue = obj;
    }
  });
});

//Funcion para retornar al main render los archivos disponibles para ser reproducidos en todo momento
ipcMain.on('getVideos', (event, arg) => {
  var list = [];
  var list_string = '';
  fs.readdir(root_dir + 'app/files/completed/', (err, files) => {
    files.forEach(file => {
      if (file != '.gitignore') {
        if (file.split('.').pop().toUpperCase() == 'MP4' && !isNaN(file.charAt(0))) {
          if (list_string == '') {
            list_string = file;
            var value = {
              "preview": file.split('-')[0],
              "file": file
            };
            list.push(value);
          } else {
            list_string = list_string + ';' + file;
            var value = {
              "preview": file.split('-')[0],
              "file": file
            };
            list.push(value);
          }
        }
      }

    });
    event.returnValue = list;
  })


});

//Funcion para actualizar el contenido en el player
ipcMain.on('updatePlayer', (event, arg) => {
  fs.writeFile(root_dir + "player.json", arg, 'utf8', function(err) {
    if (err) {
      return console.log(err);
    }
    event.returnValue = 'Actualizado';
  });
});

//Funcion para hacer upgrade al winbox
ipcMain.on('upgradeWinbox', (event, arg) => {
  fs.readFile(root_dir + 'player.json', 'utf8', function readFileCallback(err, data) {
    if (err) {
      console.log(err);
    } else {
      if (data == '') {
        obj = JSON.parse('{"block":0,"media":0}');
      } else {
        obj = JSON.parse(data);
      }
      event.returnValue = obj;
    }
  });
});

//Funcion que actualiza que imagen inferior se esta mostrando
ipcMain.on('imagenesInferiores', (event, arg) => {
  fs.writeFile(root_dir + "inferior.json", arg, 'utf8', function(err) {
    if (err) {
      return console.log(err);
    }
    event.returnValue = 'Inferiores';
  });
});

//Funcion que actualiza que imagen lateral se esta mostrando
ipcMain.on('imagenesLaterales', (event, arg) => {
  fs.writeFile(root_dir + "lateral.json", arg, 'utf8', function(err) {
    if (err) {
      return console.log(err);
    }
    event.returnValue = 'Laterales';
  });
});

//Funcion para indicar al sistema que el winbox ha sido activado y debe descargar la configuracion
ipcMain.on('activateWinbox', (event, arg) => {
  fs.writeFile(root_dir + "config.json", arg, 'utf8', function(err) {
    if (err) {
      return console.log(err);
    }
    event.returnValue = 'Winbox Activated';
  });
});

//Funcion que retorna al main render la resolucion actual del aplicativo
ipcMain.on('screen', (event, arg) => {
  const {
    width,
    height
  } = electron.screen.getPrimaryDisplay().workAreaSize;
  var screen = {
    "width": width,
    "height": height
  }
  event.returnValue = screen;
});

//Funcion para actualizar el archivo de comunicados que ya han sido reproducidos
ipcMain.on('updateComunicado', (event, arg) => {
  fs.readFile(root_dir + 'comunicados.json', 'utf8', function readFileCallback(err, data) {
    if (err) {
      console.log(err);
    }
    objC = JSON.parse(data);
    console.log(arg);
    var comunicadosJSON = [];
    objC.comunicados.forEach(function(comunicado, index) {
      if (comunicado.id == arg) {
        comunicado.reproducido = 'si';
      }
      comunicadosJSON.push(comunicado);
    });
    var comunicadosJSON = {
      "comunicados": comunicadosJSON
    };
    fs.writeFile(root_dir + "comunicados.json", JSON.stringify(comunicadosJSON), 'utf8', function(err) {
      if (err) {
        event.returnValue = err;
      }
      event.returnValue = 'Ok';
    });
  });
});
