// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron');
var WebSocket = require('ws');
const fs = require('fs');
const needle = require('needle');
var shelljsFfi = require("shelljs-ffi");
const pathIma = require('path');
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
const root_dir = `${__dirname}/`;

function iniciar () {
  // Create the browser window.
  /*mainWindow = new BrowserWindow({width: 800, height: 600})

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })*/

  var idWinbox;
  fs.readFile('/home/winbox/projectwindowschannel/config.json', 'utf8', function readFileCallback(err, data){
      idWinbox = JSON.parse(data);
      idWinbox = idWinbox.winbox.id;
      console.log("winbox: "+ idWinbox);
      ws = new WebSocket('ws://websocket.windowschannel.net/'+idWinbox+"-c");
      ws.on('close', function() {
          console.log('close ws');
      });
      ws.on('error', function() {
          console.log('error ws');
      });
      ws.on('open', function() {
         console.log('open ws');
      });

      ws.on('message', function(message) {
        obj = JSON.parse(message);
        if(obj.type == 'changeAudio') {
            console.log("cambiar audio");
            if(obj.msg == 'ANALOGICO') {
            ejecutaComandoTerminal('pacmd set-card-profile 0 output:analog-stereo');
          }
          if(obj.msg == 'DIGITAL') {
            ejecutaComandoTerminal('pacmd set-card-profile 0 output:hdmi-stereo');
          }
        }

        if(obj.type == 'restart') {
          console.log("reiniciar");
          ejecutaComandoTerminal('reboot');
        }

        if(obj.type == 'screenSignalON') {
          console.log("on ");
          shelljsFfi.exec('export DISPLAY=:0 && xset dpms force on'); 
        }

        if(obj.type == 'screenSignalOFF') {
          console.log("off ");
          shelljsFfi.exec('export DISPLAY=:0 && xset dpms force off'); 
        }

        if(obj.type == 'screenshot') {
          console.log("screenshot");
          shelljsFfi.exec('gnome-screenshot -f '+idWinbox+'.jpg');
          enviaIMG(idWinbox, idWinbox+'.jpg');     
        }
        
        if(obj.type == 'cerrarApp'){
            console.log("cierra app");
            app.exit(0);
        }
      });
  });
}

function ejecutaComandoTerminal(comando) {
  shelljsFfi.exec(comando,{async: true ,silent: true });     
}

//Funcion para enviar capturas de pantalla al CMS
function enviaIMG(idSetBox, imagen) {
   
    //lee la imagen
 setTimeout(function () {
    fs.readFile(imagen, (err, data)=>{        
      //error handle
      if(err) {console.log("enviaIMG resp crear imagen "+err+" "+resp);}
      

      //get image file extension name
      let extensionName = pathIma.extname('/home/winbox/projectwindowschannel/'+imagen);
      
      //convert image file to base64-encoded string
      let base64Image = new Buffer(data, 'binary').toString('base64');
       var datos = {

          idSetbox: idSetBox,

          captura: base64Image 

       };

      console.log("enviaIMG img"+imagen);

      needle.post("http://windowschannel.net/wsReportes.capturasPantalla", datos, null, function(err, resp) {
        var idWinbx = idSetBox+"-back";
        var errores = '';
        if(err) {
          errores = err;
        }
        console.log("hora de llamar al back: "+idWinbx);
        ws2 = new WebSocket('ws://websocket.windowschannel.net/0001');
        ws2.on('open', function() {
          console.log("open2 ws");          
          var datosEnvio = {
            "type": "respuestaEnvioIMGwinbox",
            "msg": errores,
            "id": idWinbx,
            "duration": "",
            "color": ""
          };
          console.log(JSON.stringify(datosEnvio));
          ws2.send(JSON.stringify(datosEnvio));
          ws2.close();
        });
        ws2.on('close', function() {
          console.log('close2 ws');
        });
        ws2.on('error', function(err) {
          console.log(err);
        });

      });        

    });
  }, 2000);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function () {
  iniciar()
})
// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    iniciar()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
