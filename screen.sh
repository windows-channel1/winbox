gtf 1280 720 60 | grep "Modeline .*" | sed -e "s/  Modeline //g" | xargs xrandr --newmode; xrandr | grep ".* connected" | sed -e "s/ connected.*//g" | tr -d "
" | xargs -0 -I name xrandr --addmode name 1280x720_60.00; xrandr | grep ".* connected" | sed -e "s/ connected.*//g" | tr -d "
" | xargs -0 -I name xrandr --output name --mode 1280x720_60.00;