const utilidades = require('./utilidades.js');
/*global $ */
var CryptoJS = require("crypto-js");
var {
	ipcRenderer,
	remote
} = require('electron'); //Inyeccion que permite la conexion del front con el back
const {webFrame} = require('electron');

ipcRenderer.on('prueba_date', (event) => { //Funcion que indica al back que todavia esta activo
	var e = angular.element(document.querySelector('#MESSAGES'));
	var response_sent = {
		date: new Date(),
		message: e
	};
	var response = ipcRenderer.sendSync('prueba_date_devuelta', response_sent);
});

//variable para comprobar si toca actualizar
var canales = -1;
var canalesAnt = -1;

//Inicializacion del aplicativo angular
var app = angular.module('app', ['ngRoute', 'ngMedia', 'ngDialog', 'datatables', 'ngSanitize', 'angularMoment', 'ngResource']);

//Se configuran solo dos rutas: La ruta principal donde se cargara el aplicativo principal y la ruta para activación
app.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
		when('/', {
			templateUrl: './views/first.html',
			controller: 'winboxCtrl'
		}).
		when('/initiateWinbox', {
			templateUrl: './views/initiate.html',
			controller: 'initiateCtrl'
		});
}]);

//Se define una directiva para mostrar el archivo de logs correspondiente al dia
app.directive('logs', ['$timeout', function($timeout) {
	var date = new Date();
	var dateFormated = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
	return {
		templateUrl: './log_' + dateFormated + '.log'
	}
}]);

Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
	get: function() {
		return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
	}
});

//Controlador para la activacion del winbox
app.controller('initiateCtrl', ['$scope', '$location', '$http', 'ngDialog', function($scope, $location, $http, ngDialog) {

	//Se definen los valores por defecto
	$scope.valorOpcion = 'Production';
	$scope.webpage = 'http://windowschannel.net';
	$scope.license = '';
	$scope.version = '';
	$scope.btnActivate = false;
	$scope.prePage = 'winbox'

	$http.get('../system.json').then(function(system) {
		$scope.macAdress = system.data.network.mac;
		console.log("macaddress puesta" + $scope.macAdress);
	});

	//Funcion inicial que muestra el modal para la activacion del winbox
	$scope.init = function() {
		ngDialog.open({
			template: 'views/modal-activation.html',
			className: 'ngdialog-theme-default',
			controller: 'initiateCtrl',
			showClose: false,
			closeByDocument: false
		});
	}

	//Funcion que se ejecuta cuando se cambia el servidor para la activacion cambia los valores de la url
	$scope.changeWebapge = function(type) {
		if (type == 'p') { //Produccion
			$scope.valorOpcion = 'Production';
			$scope.webpage = 'http://windowschannel.net';
			$scope.prePage = 'winbox'
		} else if (type == 'l') { // Pruebas en servidor
			$scope.valorOpcion = 'Test';
			$scope.webpage = 'http://angularpage.windowschannel.net';
			$scope.prePage = 'main'
		} else { //Desarrollo
			$scope.valorOpcion = 'Development';
			$scope.webpage = 'http://wchannel.windowschannelfactory.com';
			$scope.prePage = 'winbox'
		}
	}

	//Funcion que se ejecuta al momento de hacer click al boton de activar
	$scope.activate = function() {
		$scope.btnActivate = true;
		var host = $scope.webpage;
		var license = $scope.license;
		var version = "15.0.0";
		var content = {
			"licencia": license,
			"version": version
		};
		if (license.length == '27') { //verifica que la licencia este correcta
			$http({ //Solicitud al cms para verificar la activacion
				method: "POST",
				url: host + "/" + $scope.prePage + "/activar",
				params: content,
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function(resultPost) {
				if (resultPost.data.success) { //La activacion del winbox fue con exito
					$scope.btnActivate = false;
					var content = {
						"id": resultPost.data.result.id,
						"license": license,
						"host": $scope.webpage
					};
					var response = ipcRenderer.sendSync('activateWinbox', angular.toJson(content)); //Se envia resultado al back de la activacion
					$scope.resultActivate = 'Winbox successfully activated';
					ngDialog.open({
						template: 'views/result.html',
						className: 'ngdialog-theme-default result-modal',
						closeByDocument: false,
						scope: $scope,
						showClose: false
					});
					setTimeout(function() {
						ipcRenderer.send('async', '1');
					}, 3000); //Se envia orden para reinicio y que descargue la configuracion
				} else { //Errores en la activacion
					if (resultPost.data.code == '1') {
						$scope.btnActivate = false;
						$scope.resultActivate = 'Wrong key license';
						ngDialog.open({
							template: 'views/result.html',
							className: 'ngdialog-theme-default result-modal',
							closeByDocument: false,
							scope: $scope
						});
						$scope.btnActivate = false;
					} else {
						$scope.btnActivate = false;
						$scope.resultActivate = 'Key license has already been activated';
						ngDialog.open({
							template: 'views/result.html',
							className: 'ngdialog-theme-default result-modal',
							closeByDocument: false,
							scope: $scope
						});
						$scope.btnActivate = false;
					}
					console.log('Error');
				}
			}, function(response) {
				$scope.resultActivate = "Network error";
				ngDialog.open({
					template: 'views/result.html',
					className: 'ngdialog-theme-default result-modal',
					closeByDocument: false,
					scope: $scope
				});
				$scope.btnActivate = false;
			});
		} else {
			$scope.resultActivate = 'Wrong key license';
			ngDialog.open({
				template: 'views/result.html',
				className: 'ngdialog-theme-default result-modal',
				closeByDocument: false,
				scope: $scope
			});
			$scope.btnActivate = false;
		}
	}

}]);

//Controlador principal del aplicativo donde se generan todas las funciones del mismo
app.controller('winboxCtrl', ['$scope', '$location', '$http', 'ngDialog', 'moment', 'amMoment', '$sce', function($scope, $location, $http, ngDialog, moment, amMoment, $sce) {

	console.log(document.getElementById('video_player'));
	console.log('ldld');

	//Valores por defecto
	$scope.inferiorwidth = 0;
	$scope.lateralwidth = 0;
	$scope.inferiorheight = 0;
	$scope.lateralheight = 0;
	$scope.showClass = 2;
	$scope.keyUp = null;
	$scope.keyDown = null;
	$scope.hometab = true;
	$scope.lateralActivated = false;
	$scope.internetConection = 'inactive';
	$scope.forecastOpacity = 0;
	$scope.showforecast = 2;
	$scope.weatherActivated = false;
	$scope.weatherActivated_logo = false;
	$scope.video_demand = '';
	$scope.video_demand_replay = 0;
	$scope.weatherid = 0;
	$scope.weatherunit = '';
	$scope.logoWinchannel = 'S';
	$scope.playDemandStuck = 0;
	$scope.videoWidth = screen.width + 'px';
	$scope.videoHeight = window.innerHeight + 'px;'
	$scope.forecastLeft = screen.width - 144;
	$scope.forecasTop = (window.innerHeight * 23) / 100;
	$scope.configAnt = null;
	$scope.numRefresh = 0;
	$scope.numTest = 0;
	$scope.numDesactivacionNuestra = 0;

	$http.get('../news.json') //Obtiene las noticias que se han cargado y las agrega a la variable de noticias de la primera franja
		.then(function(news) {
			$scope.newsPropias = news.data.newsPropias;
			var changed = news.data.news.replace(/\n/g, '<br>');
			$scope.rss = changed;
		});

	$http.get('../news2.json') //Obtiene las noticias que se han cargado y las agrega a la variable de noticias de la segunda franja
		.then(function(news) {
			$scope.newsPropias2 = news.data.newsPropias;
			var changed = news.data.news.replace(/\n/g, '<br>');
			$scope.rss2 = changed;
		});

	$http.get('../comunicados.json') //Se obtiene el archivo de configuracion de los comunicados y de encontrar uno que este por reproducir lo agrega a la cola de primero
		.then(function(comunicados) {
			$scope.reproducirComunicado = 0;
			$scope.comunicadoValue = '';
			$scope.comunicadoType = '';
			comunicados.data.comunicados.forEach(function(comunicado, index) {
				if (comunicado.reproducido == 'no' && $scope.reproducirComunicado == 0) {
					$scope.reproducirComunicado = 1;
					$scope.idComunicado = comunicado.id;
					$scope.comunicadoValue = 'comunicado_' + comunicado.id + '_' + comunicado.value.split('/').pop();
					$scope.comunicadoType = comunicado.type;
					if ($scope.comunicadoType == 'image' || $scope.comunicadoType == 'text' || $scope.comunicadoType == 'streaming') {
						$scope.comunicadoDuration = comunicado.duration;
					}
					if ($scope.comunicadoType == 'text' || $scope.comunicadoType == 'streaming') {
						$scope.comunicadoValue = comunicado.value;
					}
					if (comunicado.text != 'SIN-TEXTO') {
						$scope.comunicadoText = comunicado.text;
					} else {
						$scope.comunicadoText = 'SIN-TEXTO';
					}
				}
			})
		});

	$scope.biblioteca_videos = ipcRenderer.sendSync('getVideos', '1'); //Funcion que guarda un string con los videos descargados en disco
	//Se empieza por la configuracion inicial
	$http.get('../config.json').then(function(config) {
		$scope.config = config.data;
		if (typeof (config.data.host) === "undefined") {
			$location.path("/initiateWinbox");
		}

		canales = typeof (config.data.channel.blocks);
		if (canales === "undefined") {
			canalesAnt = canales;
			$scope.refresh();
		}

		//ordena los blocks
		config.data.channel.blocks = $scope.sortBlocks(config.data.channel.blocks);

		var tieneMedia = config.data.channel.blocks[0].media.length;
		if (tieneMedia > 0) {
			var content = {
				block: 0,
				media: 0
			};
		} else {
			var content = $scope.siguienteBloqueValido(config.data.channel.blocks, 0);
		}
		var response = ipcRenderer.sendSync('updatePlayer', angular.toJson(content)); //Se envia al back que se empezara por el primer contenido

		//Se verifica que no haya ningun video enviado a distancia para ser reproducido, estos videos tienen prioridad sobre los demas
		if (typeof (config.data.videosDistancia) != "undefined" && config.data.videosDistancia != '0-0') {
			var video = config.data.videosDistancia.split('-');
			$scope.video_demand = video[0];
			$scope.video_demand_replay = video[1];
			$scope.video_distancia = 1;
			$scope.test();
		}

		$scope.updateSystem(); //Se llama a la funcion que asignara los valores de la configuracion del sistema (ip, mac, conexion disco duro, etc)

		if (typeof (config.data.host) !== "undefined") {
			setInterval(function() {
				$scope.refresh();
			}, 60000); //Se indica que cada minuto debe  verificar la nueva configuracion

			//Se asignan los valores
			$scope.host = config.data.host; //Servidor al cual esta conectado
			$scope.license = config.data.winbox.license; //Licencia
			$scope.version = config.data.winbox.version; //version del aplicativo

			$scope.color_letra = config.data.winbox.color_letra; //Color de letra de la Franja Principal
			$scope.color_fondo = config.data.winbox.color_fondo; //Color de fondo de la Franja Principal
			$scope.logoWinchannel = config.data.imagenw; //Se asigna si la imagen de windows channel debe aparecer


			//Se verifica si las noticias han sido activadas, de no ser asi se guarda como vacio las noticias
			if (typeof (config.data.channel) != "undefined" && typeof (config.data.channel.rss) != "undefined" && typeof (config.data.channel.rss[0]) != "undefined" && config.data.channel.rss[0].enabled == false && $scope.rss == '') {
				$scope.rss = '';
			}
			//Se verifica si las noticias han sido activadas, de no ser asi se guarda como vacio las noticias y se quita el color de la franja
			if (typeof (config.data.channel) != "undefined" && typeof (config.data.channel.rss2) != "undefined" && typeof (config.data.channel.rss2[0]) != "undefined" && config.data.channel.rss2[0].enabled == false && $scope.rss2 == '') {
				$scope.rss2 = '';
			} else {
				if (typeof (config.data.channel.rss2) != "undefined" && typeof (config.data.channel.rss2[0]) != "undefined" && config.data.channel.rss2[0].enabled == true && $scope.rss2 != '') {
					$scope.rss2_letra = config.data.channel.rss2[0].color_font;
					$scope.rss2_fondo = config.data.channel.rss2[0].color;
				}
			}
			//Se asigna configuracion de rss (orientacion y velocidad)
			if (typeof (config.data.channel) != "undefined" && typeof (config.data.channel.rss) != "undefined" && typeof (config.data.channel.rss[0]) != "undefined") {
				if (config.data.channel.rss[0].dir == 'vertical') {
					$scope.dirNews = 'up';
					$scope.dirnewsdirection = 'up';
					$scope.speed = '1';
				} else {
					$scope.dirNews = 'left';
					$scope.dirnewsdirection = 'left';
					$scope.speed = '4';
				}
			}

			//Se verifica si no existe una url de streaming configurada en el canal
			if (typeof (config.data.channel.url) != "undefined" && config.data.channel.url.enabled == true) {
				$scope.url_streaming = config.data.channel.url.value;
			} else {
				$scope.url_streaming = '';
			}
			//Se asigna primer video o contenido a reproducir
			if (typeof (config.data.channel.blocks) != "undefined" && config.data.channel.blocks.length > 0) { //Se verifica que existan bloques de contenidos asignados
				//Contenido video
				if (config.data.channel.blocks[content.block].media[content.media].type == 'video' && $scope.url_streaming == '') {
					if (config.data.channel.blocks[content.block].media[content.media].text != 'SIN-TEXTO') { //Se verifica si es video con texto
						$scope.typePlayer = 'videoText';
						$scope.videoTexto = $sce.trustAsHtml(config.data.channel.blocks[content.block].media[content.media].text); //Se identifica el html como seguro
					} else {
						$scope.typePlayer = 'video';
					}

					//Se debe hacer la verificacion si el contenido esta en el servidor antiguo o el nuevo
					if ((typeof (config.data.channel.blocks[content.block].media[content.media].value_new) != undefined && config.data.channel.blocks[content.block].media[content.media].value_new != null) && (config.data.channel.blocks[content.block].media[content.media].value_new != '0')) {
						$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].value_new.split('/')[7];
						console.log('0 >> $scope video -- cambio de video');
					} else {
						$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].id + '-' + config.data.channel.blocks[content.block].media[content.media].value.split('/')[3];
						console.log('1 >> $scope video -- cambio de video');
					}

				} else if (config.data.channel.blocks[content.block].media[content.media].type == 'streaming' || $scope.url_streaming != '') {
					//El contenido es un streaming
					$scope.typePlayer = 'streaming';
					if ($scope.url_streaming != '') {
						$scope.video = $sce.trustAsResourceUrl($scope.url_streaming);
						console.log('2 >> $scope video -- cambio de video');
						//Al ser un contenido streaming y no tener una duracion dija se debe asignar un timeout con la duracion colocada en el cms
						setTimeout(function() {
							$scope.test();
						}, 30000);
					} else {
						$scope.video = $sce.trustAsResourceUrl(config.data.channel.blocks[content.block].media[content.media].value);
						console.log('3 >> $scope video -- cambio de video');
						//Al ser un contenido streaming y no tener una duracion dija se debe asignar un timeout con la duracion colocada en el cms
						setTimeout(function() {
							$scope.test();
						}, config.data.channel.blocks[content.block].media[content.media].duration * 1000);
					}

				} else if (config.data.channel.blocks[content.block].media[content.media].type == 'image' && $scope.url_streaming == '') {
					//El contenido es una imagen
					if (config.data.channel.blocks[content.block].media[content.media].text != 'SIN-TEXTO') { //Se verifica si es imagen con texto
						$scope.typePlayer = 'imageText';
						$scope.imageTexto = $sce.trustAsHtml(config.data.channel.blocks[content.block].media[content.media].text); //Se identifica el html como seguro
					} else {
						$scope.typePlayer = 'image';
					}

					//Se debe hacer la verificacion si el contenido esta en el servidor antiguo o el nuevo
					if ((typeof (config.data.channel.blocks[content.block].media[content.media].value_new) != undefined && config.data.channel.blocks[content.block].media[content.media].value_new != null) && (config.data.channel.blocks[content.block].media[content.media].value_new != '0')) {
						$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].value.split('/')[7];
						console.log('4 >> $scope video -- cambio de video');
					} else {
						$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].id + '-' + config.data.channel.blocks[content.block].media[content.media].value.split('/')[3];
						console.log('5 >> $scope video -- cambio de video');
					}
					//Al ser un contenido imagen y no tener una duracion dija se debe asignar un timeout con la duracion colocada en el cms
					setTimeout(function() {
						$scope.test();
					}, config.data.channel.blocks[content.block].media[content.media].duration * 1000);
				} else if (config.data.channel.blocks[content.block].media[content.media].type == 'text' && $scope.url_streaming == '') {
					//El contenido es texto
					$scope.typePlayer = 'onlyText';
					$scope.textJustText = $sce.trustAsHtml(config.data.channel.blocks[content.block].media[content.media].value); //Se identifica el html como seguro
					$scope.video = '';
					console.log('6 >> $scope video -- cambio de video');
					//Al ser un contenido texto y no tener una duracion dija se debe asignar un timeout con la duracion colocada en el cms
					setTimeout(function() {
						console.log('5');
						$scope.test();
					}, config.data.channel.blocks[content.block].media[content.media].duration * 1000);
				}

				//Se carga el logo del canal
				if (config.data.channel.logo.type == 'image') {
					$scope.logo_channel = './files/completed/logo_channel_' + config.data.channel.logo.value.split('/').pop();
					$scope.logo_channel_image = true;
				} else {
					$scope.logo_channel = config.data.channel.logo.value;
					$scope.logo_channel_image = false;
				}

				//Se carga la imagene de decoracion del canal
				if (config.data.channel.background.enabled == true) {
					$scope.background_channel = './files/completed/background_channel_' + config.data.channel.background.value.split('/').pop();
				} else {
					$scope.background_channel = ' ';
				}
			} else {

				if ($scope.url_streaming != '') {
					$scope.video = $sce.trustAsResourceUrl($scope.url_streaming);
					console.log('7 >> $scope video -- cambio de video');
					setTimeout(function() {
						$scope.test();
					}, 30000);
				} else {
					$scope.typePlayer = '';
					$scope.video = '';
					console.log('8 >> $scope video -- cambio de video');
				}
			}

			//Se carga la imagen del cliente
			if (config.data.client_logo.type == 'image') {
				$scope.client_logo = './files/completed/client_logo_' + config.data.client_logo.value.split('/').pop();
				$scope.client_logo_image = true;
			} else {
				$scope.client_logo = config.data.client_logo.value;
				$scope.client_logo_image = false;
			}

			//Se verifican los datos de la hora, formato, que mostrar y timezone
			if (config.data.timezone == '') {
				$scope.timezone = 'America/Bogota';
			} else {
				$scope.timezone = config.data.timezone;
			}
			if (config.data.date_format == '') {
				$scope.dateFormat = '';
			} else {
				$scope.dateFormat = config.data.date_format;
			}
			if (config.data.time_format == '') {
				$scope.timeFormat = '';
			} else {
				$scope.timeFormat = config.data.time_format;
			}
			if (config.data.date_lang == '') {
				amMoment.changeLocale('es');
			} else {
				amMoment.changeLocale(config.data.date_lang);
			}
			$scope.exampleDate = moment(); //Se utiliza momentjs para el manejo de las fechas

			//BANNERS INICIO

			//Se empieza la verificacion de la configuracion de las imagenes laterales e inferiores
			var number = Math.floor((Math.random() * 100) + 1); //Se define un numero al azar para asignar al src de los archivos, de esta forma se evita problemas del
			if (typeof (config.data.channel.lateral) != "undefined") {
				//Se verifica que las imagenes laterales no esten habilitados
				if (config.data.channel.lateral.enabled == true) {
					$scope.showLateral = true;
				} else {
					$scope.showLateral = false;
				}

				//Se verifica que las imagenes inferiores no esten habilitados
				if (config.data.channel.lateral.bottom_enabled == true) {
					$scope.showInferior = true;
				} else {
					$scope.showInferior = false;
				}

				//Si las imagenes laterales ya se encuentran activadas se salta este paso
				if (config.data.channel.lateral.enabled == true && $scope.lateralActivated == false) {
					if (typeof (config.data.channel.imagenesLaterales[0]) != "undefined") { //Se verifica que exista al menos una imagen lateral y se asigna
						if (config.data.channel.imagenesLaterales[0].picture_new == -1) {
							$scope.imagenLateral = './files/completed/lateral_image_' + config.data.channel.imagenesLaterales[0].picture.split('/')[3] + '?decache=' + number;
						} else {
							$scope.imagenLateral = './files/completed/lateral_image_' + config.data.channel.imagenesLaterales[0].picture.split('/')[3] + '?decache=' + number;

						}
					}
					if (typeof (config.data.channel.imagenesInferiores[0]) != "undefined") { //Se verifica que exista al menos una imagen inferior y se asigna
						if (config.data.channel.imagenesInferiores[0].picture_new == -1) {
							$scope.imagenInferior = './files/completed/bottom_image_' + config.data.channel.imagenesInferiores[0].picture.split('/')[3] + '?decache=' + number;
						} else {
							$scope.imagenInferior = './files/completed/bottom_image_' + config.data.channel.imagenesInferiores[0].picture.split('/')[3] + '?decache=' + number;
						}
					}

					//Se asigna duracion e intervalo de las imagenes
					var duration = parseInt(config.data.channel.lateral.duration) * 1000;
					var interval = parseInt(config.data.channel.lateral.interval) * 1000;

					//ordena los banners inferiores y laterales
					config.data.channel.imagenesLaterales = $scope.sortBanners(config.data.channel.imagenesLaterales);
					config.data.channel.imagenesInferiores = $scope.sortBanners(config.data.channel.imagenesInferiores);

					//Activar imagenes laterales e inferiore
					$scope.lateralActivated = true;
					setTimeout(function() {
						$scope.toogleBanner();
					}, 3000); //Funcion que muestra el panel actual
					$scope.intervalApply = setTimeout(function() {
						$scope.toogleBannerApply(duration, interval);
					}, 5000); //Funcion que empieza el intervalo para el cambio de imagen, se envia duracion e intervalo
				} else { //Si se encuentra activadas las imagenes laterales se verifica que continue asi, sino se deshabilitan
					if (config.data.channel.lateral.enabled == false && $scope.lateralActivated == true) {

						clearTimeout($scope.intervalApply);
						clearTimeout($scope.intervalApplyHide);
						$scope.toogleBannerHide();
						$scope.lateralActivated = false;
						$scope.showLateral = false;
						$scope.showInferior = false;
					}
				}
			} else {
				//Se deshabilitan las imagenes para evitar que queden activas
				$scope.showLateral = false;
				$scope.showInferior = false;
				clearTimeout($scope.intervalApply);
				clearTimeout($scope.intervalApplyHide);
				$scope.toogleBannerHide();
				$scope.lateralActivated = false;
			}

			// FIN BANNERS


		}

	});

	//Funcion que recibe un mensaje local desde el back por websocket
	ipcRenderer.on('mesage_local', (event, arg) => {
		clearTimeout($scope.interval_message); //Si se esta mostrando un mensaje se actualiza por el nuevo
		$scope.message_local = true;
		$scope.mensajeEmergente = arg.data;
		$scope.message_color = arg.color;
		$scope.$apply();
		$scope.interval_message = setTimeout(function() {
			$scope.message_local = false;
			$scope.mensajeEmergente = '';
			$scope.$apply();
		}, parseInt(arg.duration) * 1000);
	});

	//Funcion para reproducir un video en demanda enviado desde el back por websocket
	ipcRenderer.on('video_demand', (event, arg) => {
		var video = arg.data.split('-');
		$scope.video_demand = video[0];
		$scope.video_demand_replay = video[1];
		$scope.video_distancia = 1;
		$scope.test();
	});

	//Funcion para reproducir un video on demand desde el aplicativo
	$scope.play_video_demand = function(file) {
		$scope.play_video_demand = 1;
		$scope.video_demand = file;
		$scope.video_demand_replay = 1;
		$scope.video_distancia = 0;
		$scope.menuId.close();
	}

	//Funcion para refrescar la configuracion del aplicativo, se ejecuta cada minuto
	$scope.refresh = function() {
		console.log('Refresh Config');
		$scope.numRefresh = $scope.numRefresh + 1;

		//Actualizar noticias
		$http.get('../news.json')
			.then(function(news) {
				if (typeof (news.data.news) != "undefined") {
					$scope.newsPropias = news.data.newsPropias;
					var changed = news.data.news.replace(/\n/g, '<br>');
					$scope.rss = changed;
				} else {
					$scope.rss = '';
				}
			});

		//Actualizar noticias 2
		$http.get('../news2.json')
			.then(function(news) {
				if (typeof (news.data.news) != "undefined") {
					$scope.newsPropias2 = news.data.newsPropias;
					var changed = news.data.news.replace(/\n/g, '<br>');
					$scope.rss2 = changed;
				} else {
					$scope.rss2 = '';
				}
			});

		$scope.updateSystem(); //Actualizar datos actuales del sistema (ram, disco duro, red)

		$scope.actualizarComunicados(); //Se verifica que no existan comunicados por reproducir, si existen se agregan a la cola

		//Se verifica si para el momento de inicializacion no habia bloque o contenidos asignados y se verifica nuevamente (show must go on)
		if ($scope.video == '' && $scope.typePlayer == '') {
			console.log('9 >> $scope video -- cambio de video');
			$scope.numTest = $scope.numTest + 1;
			$scope.test();
		}

		//Se obtiene configuracion
		$http.get('../config.json').then(function(config) {

			$scope.configAnt = $scope.config;
			$scope.config = config.data;
			canalesAnt = canales;
			canales = typeof (config.data.channel.blocks);

			//habia reproduccion y debe continuar
			if ($scope.video != null && $scope.typePlayer != null && canalesAnt != 'undefined' && canalesAnt != null) {
				//$scope.test();
			}

			if (($scope.video == null && $scope.typePlayer == null) || ($scope.video == undefined && $scope.typePlayer == undefined) || ($scope.video == '' && $scope.typePlayer == '')) {
				console.log('10 >> $scope video -- cambio de video');
				setTimeout(function() {
					$scope.refresh();
				}, 30000);
			}

			//vuelve la programación
			if (typeof ($scope.config.channel.blocks) != "undefined" && typeof ($scope.configAnt.channel.blocks) == "undefined") {
				var content = {
					block: 0,
					media: 0
				};
				var response = ipcRenderer.sendSync('updatePlayer', angular.toJson(content));
				$scope.numTest = $scope.numTest + 1;
				$scope.test();
			}

			//se ha ido la programación y los banners seguían activos
			if (($scope.showLateral == true || $scope.showInferior == true || $scope.lateralActivated == true) && typeof ($scope.config.channel.blocks) == "undefined" && typeof ($scope.configAnt.channel.blocks) != "undefined") {
				$scope.numDesactivacionNuestra = $scope.numDesactivacionNuestra + 1;
				//Se deshabilitan las imagenes para evitar que queden activas
				$scope.showLateral = false;
				$scope.showInferior = false;
				clearTimeout($scope.intervalApply);
				clearTimeout($scope.intervalApplyHide);
				$scope.toogleBannerHide();
				$scope.lateralActivated = false;
			}
			//ordena los blocks
			//config.data.channel.blocks = $scope.sortBlocks(config.data.channel.blocks);

			$scope.exampleDate = moment(); //Se actualiza hora del winbox
			$scope.color_letra = config.data.winbox.color_letra;
			$scope.color_fondo = config.data.winbox.color_fondo;
			$scope.logoWinchannel = config.data.imagenw;

			//Se verifica que no se haya enviado un video on demand para ser reproducido
			if (typeof ($scope.config.videosDistancia) != "undefined" && $scope.config.videosDistancia != '0-0') {
				var video = $scope.config.videosDistancia.split('-');
				$scope.video_demand = video[0];
				$scope.video_demand_replay = video[1];
				$scope.video_distancia = 1;
				console.log("video a distancia: " + $scope.video_demand + " rep: " + $scope.video_demand_replay);
				$scope.numTest = $scope.numTest + 1;
				$scope.test();
			}

			//Se actualiza la informacion del endpoint
			$scope.host = config.data.host;
			$scope.license = config.data.winbox.license;
			$scope.version = config.data.winbox.version;

			//Si la rss no se encientra activada se borra el valor que tenga las noticias
			if (typeof (config.data.channel) != "undefined" && typeof (config.data.channel.rss) != "undefined" && typeof (config.data.channel.rss[0]) != "undefined" && config.data.channel.rss[0].enabled == false && $scope.rss == '') {
				$scope.rss = '';
			}

			//Si la segunda rss no se encientra activada se borra el valor que tenga las noticias
			if (typeof (config.data.channel) != "undefined" && typeof (config.data.channel.rss2) != "undefined" && typeof (config.data.channel.rss2[0]) != "undefined" && config.data.channel.rss2[0].enabled == false && $scope.rss2 == '') {
				$scope.rss2 = '';
			} else {
				if (typeof (config.data.channel.rss2) != "undefined" && typeof (config.data.channel.rss2[0]) != "undefined" && config.data.channel.rss2[0].enabled == true && $scope.rss2 != '') {
					$scope.rss2_letra = config.data.channel.rss2[0].color_font;
					$scope.rss2_fondo = config.data.channel.rss2[0].color;
				}
			}

			//Se actualiza la configuracion de la rss del canal (orientacion y velocidad)
			if (typeof (config.data.channel) != "undefined" && typeof (config.data.channel.rss) != "undefined" && typeof (config.data.channel.rss[0]) != "undefined") {
				if (config.data.channel.rss[0].dir == 'vertical') {
					$scope.dirNews = 'up';
					$scope.dirnewsdirection = 'up';
					$scope.speed = '1';
				} else {
					$scope.dirNews = 'left';
					$scope.dirnewsdirection = 'left';
					$scope.speed = '4';
				}
			}

			var getWeather = function() {
				let url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
				let method = 'GET';
				let app_id = 'palZXg6k';
				let consumer_key = 'dj0yJmk9WTZXUjRPaXhOUlRNJmQ9WVdrOWNHRnNXbGhuTm1zbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWU1';
				let consumer_secret = 'd5014aa4063aeddbce0fee62e988d42c6dfb334b';
				let concat = '&';
				let query = {
					'format': 'json',
					'woeid': config.data.weather.id,
					'u': config.data.weather.unit.toLowerCase()
				};
				let oauth = {
					'oauth_consumer_key': consumer_key,
					'oauth_nonce': Math.random().toString(36).substring(2),
					'oauth_signature_method': 'HMAC-SHA1',
					'oauth_timestamp': parseInt(new Date().getTime() / 1000).toString(),
					'oauth_version': '1.0'
				};

				let merged = {};
				$.extend(merged, query, oauth);
				// Note the sorting here is required
				let merged_arr = Object.keys(merged).sort().map(function(k) {
					return [k + '=' + encodeURIComponent(merged[k])];
				});
				let signature_base_str = method +
					concat + encodeURIComponent(url) +
					concat + encodeURIComponent(merged_arr.join(concat));

				let composite_key = encodeURIComponent(consumer_secret) + concat;
				let hash = CryptoJS.HmacSHA1(signature_base_str, composite_key);
				let signature = hash.toString(CryptoJS.enc.Base64);

				oauth['oauth_signature'] = signature;
				let auth_header = 'OAuth ' + Object.keys(oauth).map(function(k) {
					return [k + '="' + oauth[k] + '"'];
				}).join(',');

				// $.ajax({
				// 	url: url + '?' + $.param(query),
				// 	headers: {
				// 		'Authorization': auth_header,
				// 		'X-Yahoo-App-Id': app_id
				// 	},
				// 	method: 'GET',
				// 	success: function (data) {
				// 		console.log(data);
				// 	}
				// });

				return $http.get(url + '?' + $.param(query), {
					headers: {
						'Authorization': auth_header,
						'X-Yahoo-App-Id': app_id
					}
				})
			}

			//Se verifica la configuracion del clima del equipo
			if (typeof (config.data.weather) != "undefined" && $scope.showLateral != true) { //Se verifica si el clima esta configurado y si la imagen lateral no esta activada
				//Se verifica: Si el id de clima este asignado, que el clima no este activo en este momento y que la duracion e intervalo del panel de clima esten asignados
				if (((config.data.weather.id != 0 && $scope.weatherActivated == false) || $scope.weatherid != config.data.weather.id) && (config.data.weather.forecast_duration != 0 && config.data.weather.forecast_interval != 0)) {
					$scope.weatherid = config.data.weather.id;
					$scope.weatherunit = config.data.weather.unit;
					$scope.weatherunit = '°' + $scope.weatherunit;
					var q = "select * from weather.forecast where woeid%3D" + config.data.weather.id + " and u%3D'" + config.data.weather.unit.toLowerCase() + "'";
					var url = "https://query.yahooapis.com/v1/public/yql?q=" + q.replace(" ", "%20") + "&format=json";
					// console.log(url);



					// getWeather().then(function (weather) {
					// 	console.log(weather);
					// });

					getWeather().then(function(weather) { //Se crea y realiza la consulta a el api de yahoo para obtener la informacion del clima por el codigo WOOEID
						try {
							console.log(weather);
							$scope.weatherActualImg = weather.data.current_observation.condition.code; //Se asigna el codigo del estado del clima actual
							$scope.weatherActualTemp = weather.data.current_observation.condition.temperature + $scope.weatherunit;

							//Primer elemento del panel de clima
							$scope.weatherFirstImg = weather.data.forecasts[1].code;
							$scope.weatherFirstDate = moment.unix(weather.data.forecasts[1].date).format("dddd").toUpperCase();
							$scope.weatherFirstTemp = weather.data.forecasts[1].low + $scope.weatherunit + ' - ' + weather.data.forecasts[1].high + $scope.weatherunit;

							//Segundo elemento del panel de clima
							$scope.weatherSecondImg = weather.data.forecasts[2].code;
							$scope.weatherSecondDate = moment.unix(weather.data.forecasts[2].date).format("dddd").toUpperCase();
							$scope.weatherSecondTemp = weather.data.forecasts[2].low + $scope.weatherunit + ' - ' + weather.data.forecasts[2].high + $scope.weatherunit;

							//Tercer elemento del panel de clima
							$scope.weatherThirdImg = weather.data.forecasts[3].code;
							$scope.weatherThirdDate = moment.unix(weather.data.forecasts[3].date).format("dddd").toUpperCase();
							$scope.weatherThirdTemp = weather.data.forecasts[3].low + $scope.weatherunit + ' - ' + weather.data.forecasts[3].high + $scope.weatherunit;
						} catch (err) {
							console.log(err);
						}

						//Se indica que el logo de clima debe ser mostrado y se activa, tambien se indica cada cuanto debe ser ocultado y mostrado el clima
						$scope.weatherActivated_logo = true;
						$scope.weatherActivated = true;
						$scope.intervalApplyWeather = setTimeout(function() {
							$scope.toogleWeather((config.data.weather.forecast_duration * 1000), (config.data.weather.forecast_interval * 1000));
						}, 5000);

					}, function(response) {
						console.log('Weather error');
					});
				} else {
					//Se entiende que el panel de clima ya esta activado y se verifica si continua asi, de no ser asi se deshabilita el panel de clima
					if (config.data.weather.forecast_duration != 0 && config.data.weather.forecast_interval != 0) {

						//comprueba que no hayan cambiado las unidades de temperatura
						if ($scope.weatheruni != config.data.weather.unit) {
							$scope.weatherid = config.data.weather.id;
							$scope.weatherunit = config.data.weather.unit;
							$scope.weatherunit = '°' + $scope.weatherunit;
							var q = "select * from weather.forecast where woeid%3D" + config.data.weather.id + " and u%3D'" + config.data.weather.unit.toLowerCase() + "'";
							var url = "https://query.yahooapis.com/v1/public/yql?q=" + q.replace(" ", "%20") + "&format=json";
							getWeather().then(function(weather) {
								$scope.weatherActualImg = weather.data.current_observation.condition.code;
								$scope.weatherActualTemp = weather.data.current_observation.condition.temperature + $scope.weatherunit;
								$scope.weatherActivated_logo = true;
							}, function(response) {
								console.log('Weather error');
							});
						}
						if ($scope.weatherActivated == true && (config.data.weather.id == 0 || config.data.weather.forecast_enabled == false)) {
							$scope.weatherActivated = false;
							$scope.weatherActivated_logo = false;
							clearTimeout($scope.intervalApplyWeather);
							clearTimeout($scope.intervalApplyWeatherHide);
							$scope.weatherActualImg = '';
							$scope.weatherActualTemp = '';
							$scope.hideWeather();
						}
					} else {
						//Se indica que el panel de clima no debe ser activado, sin embargo se verifica si el logo de clima debe mostrarse
						$scope.weatherActivated = false;
						if (typeof (config.data.weather) != "undefined" && config.data.weather.id != 0) {
							$scope.weatherid = config.data.weather.id;
							$scope.weatherunit = config.data.weather.unit;
							$scope.weatherunit = '°' + $scope.weatherunit;
							var q = "select * from weather.forecast where woeid%3D" + config.data.weather.id + " and u%3D'" + config.data.weather.unit.toLowerCase() + "'";
							var url = "https://query.yahooapis.com/v1/public/yql?q=" + q.replace(" ", "%20") + "&format=json";
							getWeather().then(function(weather) {
								$scope.weatherActualImg = weather.data.current_observation.condition.code;
								$scope.weatherActualTemp = weather.data.current_observation.condition.temperature + $scope.weatherunit;
								$scope.weatherActivated_logo = true;
							}, function(response) {
								console.log('Weather error');
							});
						} else {
							$scope.weatherActualImg = '';
							$scope.weatherActualTemp = '';
							$scope.weatherActivated_logo = false;
						}
						clearTimeout($scope.intervalApplyWeather);
						clearTimeout($scope.intervalApplyWeatherHide);
						$scope.hideWeather();
					}
				}
			} else {
				//Se indica que el panel de clima no debe ser activado, sin embargo se verifica si el logo de clima debe mostrarse
				if (typeof (config.data.weather) != "undefined" && config.data.weather.id != 0) {
					$scope.weatherid = config.data.weather.id;
					$scope.weatherunit = config.data.weather.unit;
					$scope.weatherunit = '°' + $scope.weatherunit;
					var q = "select * from weather.forecast where woeid%3D" + config.data.weather.id + " and u%3D'" + config.data.weather.unit.toLowerCase() + "'";
					var url = "https://query.yahooapis.com/v1/public/yql?q=" + q.replace(" ", "%20") + "&format=json";
					getWeather().then(function(weather) {
						$scope.weatherActualImg = weather.data.current_observation.condition.code;
						$scope.weatherActualTemp = weather.data.current_observation.condition.temperature + $scope.weatherunit;
						$scope.weatherActivated_logo = true;
					}, function(response) {
						console.log('Weather error');
					});
				} else {
					$scope.weatherActualImg = '';
					$scope.weatherActualTemp = '';
					$scope.weatherActivated_logo = false;
				}
				$scope.weatherActivated = false;
				clearTimeout($scope.intervalApplyWeather);
				clearTimeout($scope.intervalApplyWeatherHide);
				$scope.hideWeather();
			}

			//Se verifica que existe una configuracion valida para el winbox, sino se carga pantalla para activar winbox
			if (typeof (config.data.host) === "undefined") {
				$location.path("/initiateWinbox");
			} else {
				var number = Math.floor((Math.random() * 100) + 1); //Se define un numero al azar para asignar al src de los archivos, de esta forma se evita problemas del archivos nuevos con el mismo nombre
				//Se verifica que la nueva configuracion tenga asignado bloques
				if (typeof (config.data.channel.blocks) != "undefined") {
					//Se verifica nuevamente el logo del canal
					if (config.data.channel.logo.type == 'image') {
						$scope.logo_channel = './files/completed/logo_channel_' + config.data.channel.logo.value.split('/').pop() + '?decache=' + number;
						$scope.logo_channel_image = true;
					} else {
						$scope.logo_channel = config.data.channel.logo.value;
						$scope.logo_channel_image = false;
					}

					//Se verifica nuevamente la decoracion del canal
					if (config.data.channel.background.enabled == true) {
						$scope.background_channel = './files/completed/background_channel_' + config.data.channel.background.value.split('/').pop() + '?decache=' + number;
					} else {
						$scope.background_channel = ' ';
					}
				} else {
					$scope.typePlayer = '';
					$scope.video = '';
					console.log('11 >> $scope video -- cambio de video');
					$scope.$apply();
				}

				//Se verifica nuevamente la imagen del canal
				if (config.data.client_logo.type == 'image') {
					$scope.client_logo = './files/completed/client_logo_' + config.data.client_logo.value.split('/').pop() + '?decache=' + number;
					$scope.client_logo_image = true;
				} else {
					$scope.client_logo = config.data.client_logo.value;
					$scope.client_logo_image = false;
				}

				//Se configura nuevamente la configuracion de la hora (formato, que mostrar y que ocultar, timezone)
				if (config.data.timezone == '') {
					$scope.timezone = 'America/Bogota';
				} else {
					$scope.timezone = config.data.timezone;
				}
				if (config.data.date_format == '') {
					$scope.dateFormat = '';
				} else {
					$scope.dateFormat = config.data.date_format;
				}
				if (config.data.time_format == '') {
					$scope.timeFormat = '';
				} else {
					$scope.timeFormat = config.data.time_format;
				}
				if (config.data.date_lang == '') {
					amMoment.changeLocale('es');
				} else {
					amMoment.changeLocale(config.data.date_lang);
				}

				//Se empieza la verificacion de la configuracion de las imagenes laterales e inferiores
				if (typeof (config.data.channel.lateral) != "undefined") {
					//Se verifica que las imagenes laterales no esten habilitados
					if (config.data.channel.lateral.enabled == true) {
						$scope.showLateral = true;
					} else {
						$scope.showLateral = false;
					}

					//Se verifica que las imagenes inferiores no esten habilitados
					if (config.data.channel.lateral.bottom_enabled == true) {
						$scope.showInferior = true;
					} else {
						$scope.showInferior = false;
					}

					//Si las imagenes laterales ya se encuentran activadas se salta este paso
					if (config.data.channel.lateral.enabled == true && $scope.lateralActivated == false) {
						if (typeof (config.data.channel.imagenesLaterales[0]) != "undefined") { //Se verifica que exista al menos una imagen lateral y se asigna
							if (config.data.channel.imagenesLaterales[0].picture_new == -1) {
								$scope.imagenLateral = './files/completed/lateral_image_' + config.data.channel.imagenesLaterales[0].picture.split('/')[3] + '?decache=' + number;
							} else {
								$scope.imagenLateral = './files/completed/lateral_image_' + config.data.channel.imagenesLaterales[0].picture.split('/')[3] + '?decache=' + number;
							}
						}
						if (typeof (config.data.channel.imagenesInferiores[0]) != "undefined") { //Se verifica que exista al menos una imagen inferior y se asigna
							if (config.data.channel.imagenesLaterales[0].picture_new == -1) {
								$scope.imagenInferior = './files/completed/bottom_image_' + config.data.channel.imagenesInferiores[0].picture.split('/')[3] + '?decache=' + number;
							} else {
								$scope.imagenInferior = './files/completed/bottom_image_' + config.data.channel.imagenesInferiores[0].picture.split('/')[3] + '?decache=' + number;
							}
						}

						//Se asigna duracion e intervalo de las imagenes
						var duration = parseInt(config.data.channel.lateral.duration) * 1000;
						var interval = parseInt(config.data.channel.lateral.interval) * 1000;

						//Activar imagenes laterales e inferiores
						$scope.lateralActivated = true;
						setTimeout(function() {
							$scope.toogleBanner();
						}, 3000); //Funcion que muestra el panel actual
						$scope.intervalApply = setTimeout(function() {
							$scope.toogleBannerApply(duration, interval);
						}, 5000); //Funcion que empieza el intervalo para el cambio de imagen, se envia duracion e intervalo
					} else { //Si se encuentra activadas las imagenes laterales se verifica que continue asi, sino se deshabilitan
						if (config.data.channel.lateral.enabled == false && $scope.lateralActivated == true) {
							clearTimeout($scope.intervalApply);
							clearTimeout($scope.intervalApplyHide);
							$scope.toogleBannerHide();
							$scope.lateralActivated = false;
							$scope.showLateral = false;
							$scope.showInferior = false;
						}
					}
				} else {
					//Se deshabilitan las imagenes para evitar que queden activas
					if (config.data.channel.lateral.enabled == false && $scope.lateralActivated == true) {
						$scope.showLateral = false;
						$scope.showInferior = false;
						clearTimeout($scope.intervalApply);
						clearTimeout($scope.intervalApplyHide);
						$scope.toogleBannerHide();
						$scope.lateralActivated = false;
					}
				}
			}
		});

		/*
		 * Refrescar src del video si no se esta reproduciendo.
		 * Esto puede ser una solucion temporal a las pantallas en blanco.
		 */
		if (!document.getElementById('video_player').playing) {
			console.log('Refrescar video');
			let videoSrc = $scope.video;
			$scope.video = null;
			$scope.video = videoSrc;
			setTimeout(function() {
				$scope.test();
			}, 1000);
		}
		// webFrame.clearCache();
		// console.log('limpiar cache');
		// utilidades.limpiarCache();
	}

	//Funcion para enviar un comando al back
	$scope.command = function(action) {
		console.log("script: entra en accion=" + action);
		$scope.gif_loading = true;
		console.log("script: antes de llamar a async");
		ipcRenderer.sendSync('async', action);
		ipcRenderer.on('response', (event, arg) => {
			if (arg == '2') {
				$scope.resultActivate = "Error, please, try again";
				ngDialog.open({
					template: 'views/result.html',
					className: 'ngdialog-theme-default',
					scope: $scope
				});
			}
			$scope.gif_loading = false;
		});
	}

	//Esta funcion muestra el panel de clima, esta configurado con css3 para generar un efecto de aparecer, recibe duracion e intervalo
	$scope.toogleWeather = function(duration, interval) {
		$scope.forecastOpacity = 1;
		$scope.showforecast = 1;
		$scope.$apply();
		$scope.intervalApplyWeather = setTimeout(function() {
			$scope.toogleWeatherHide(duration, interval);
		}, duration);
	}

	//Esta funcion oculta el panel de clima, esta configurado con css3 para generar un efecto de desaparecer, recibe duracion e intervalo
	$scope.toogleWeatherHide = function(duration, interval) {
		$scope.forecastOpacity = 0;
		$scope.showforecast = 0;
		$scope.$apply();
		$scope.intervalApplyWeatherHide = setTimeout(function() {
			$scope.toogleWeather(duration, interval);
		}, interval);
	}

	//Funcion para terminar el proceso de ocultar y mostrar el panel de clima
	$scope.hideWeather = function() {
		$scope.forecastOpacity = 0;
		$scope.showforecast = 0;
	}

	//Funcion para actualizar los datos del sistema actual
	$scope.updateSystem = function() {
		$http.get('../system.json').then(function(system) {
			//Datos de la red
			$scope.ipAddress = system.data.network.ip;
			$scope.macAdress = system.data.network.mac;
			$scope.online = system.data.online;
			if ($scope.online) {
				$scope.internetClass = 'label-success';
				$scope.internetState = 'Conectado';
				$scope.internetConection = 'connected';
			} else {
				$scope.internetClass = 'label-danger';
				$scope.internetState = 'Desconectado';
				$scope.internetConection = 'disconnected';
			}

			//Datos del disco
			$scope.diskFreeAmount = system.data.disk.diskFreeAmount;
			$scope.diskTotalAmount = system.data.disk.diskTotalAmount;
			$scope.diskFreePer = system.data.disk.diskFreePer;
			$scope.diskUsedPer = system.data.disk.diskUsedPer;

			if (parseInt(system.data.disk.diskUsedPer) > 25 && parseInt(system.data.disk.diskUsedPer) <= 50) {
				$scope.diskClass = 'progress-bar-info';
			} else if (parseInt(system.data.disk.diskUsedPer) > 50 && parseInt(system.data.disk.diskUsedPer) <= 75) {
				$scope.diskClass = 'progress-bar-warning';
			} else if (parseInt(system.data.disk.diskUsedPer) > 75 && parseInt(system.data.disk.diskUsedPer) <= 100) {
				$scope.diskClass = 'progress-bar-danger';
			} else {
				$scope.diskClass = 'progress-bar-success';
			}

			//Datos de la memoria RAM
			$scope.ramFreeAmount = system.data.ram.ramFreeAmount;
			$scope.ramTotalAmount = system.data.ram.ramTotalAmount;
			$scope.ramFreePer = system.data.ram.ramFreePer;
			$scope.ramUsedPer = system.data.ram.ramUsedPer;

			if (parseInt(system.data.ram.ramUsedPer) > 25 && parseInt(system.data.ram.ramUsedPer) <= 50) {
				$scope.ramClass = 'progress-bar-info';
			} else if (parseInt(system.data.ram.ramUsedPer) > 50 && parseInt(system.data.ram.ramUsedPer) <= 75) {
				$scope.ramClass = 'progress-bar-warning';
			} else if (parseInt(system.data.ram.ramUsedPer) > 75 && parseInt(system.data.ram.ramUsedPer) <= 100) {
				$scope.ramClass = 'progress-bar-danger';
			} else {
				$scope.ramClass = 'progress-bar-success';
			}

		});
	}
	//Esta funcion es la encargada de cambiar los contenidos que se estan reproduciendo
	$scope.test = function(ele) {
		var response = ipcRenderer.sendSync('getCurrentBlock', '1'); //Solicitud al back del bloque actual de reproduccion
		var getCurrentBlock = response;

		var number = Math.floor((Math.random() * 100) + 1); //Numero al azar para asignar contenidos
		$http.get('../config.json').then(function(config) {

			//ordena los blocks
			config.data.channel.blocks = $scope.sortBlocks(config.data.channel.blocks);

			if (config.data.channel.blocks.length > 0) { //Se verifica que existen bloques asignados
				var exist = config.data.channel.blocks[getCurrentBlock.block].media[getCurrentBlock.media + 1];
				var aux = getCurrentBlock.block + 1;
				// ************* EXISTE EL SIGUIENTE VIDEO DEL MISMO BLOQUE?
				if (typeof (exist) === "undefined") { //Se verifica que existe un siguiente contenido
					//SI NO EXISTE EL SIGUIENTE VIDEO DEL MISMO BLOQUE
					var existBlock = config.data.channel.blocks[getCurrentBlock.block + 1];
					//MIRAMOS SI EXISTE EL SIGUIENTE BLOQUE
					if (typeof (existBlock) === "undefined") { //Se verifica si existe un siguiente bloque PENDIENTE;
						var content = $scope.siguienteBloqueValido(config.data.channel.blocks, getCurrentBlock.block);
					} else {
						var tieneMedia = config.data.channel.blocks[getCurrentBlock.block + 1].media.length;
						if (tieneMedia > 0) {
							var resultadoSuma = getCurrentBlock.block + 1;
							var content = { //Se asigna siguiente bloque en el primer contenido
								block: resultadoSuma,
								media: 0
							};
						} else {
							var content = $scope.siguienteBloqueValido(config.data.channel.blocks, getCurrentBlock.block + 1);
						}
					}
				} else {
					// ***************** SI EXISTE EL SIGUIENTE VIDEO DEL MISMO BLOQUE, ADELANTE
					var resultadoSuma = getCurrentBlock.media + 1;
					var content = { //Se asigna siguiente contenido
						block: getCurrentBlock.block,
						media: resultadoSuma
					};
				}
			}
			// TODO TODO
			var response = ipcRenderer.sendSync('updatePlayer', angular.toJson(content)); //Se actualiza el contenido que se esta reproduciendo actualmente
			//Primero se verifica que no haya un contenido on demand para reproducir
			if ($scope.video_demand_replay >= 1 && $scope.playDemandStuck == 0) {
				$scope.playDemandStuck = 1; //Indica que se esta reproduciendo un video on demand
				if ($scope.video_distancia == '1') {
					$scope.biblioteca_videos = ipcRenderer.sendSync('getVideos', '1');
					$scope.biblioteca_videos.forEach(elem => {
						if (elem.file.split('-')[0] == $scope.video_demand) { //Se revisa que contenido es el que se debe reproducir
							$scope.video_demand = elem.file;
						}
					});
				}

				$scope.resultActivate = "Video se reproducira en 3 segundos";
				var dialog = ngDialog.open({
					template: 'views/result.html',
					className: 'ngdialog-theme-default',
					scope: $scope,
					showClose: false,
				});

				setTimeout(function() { //Despues de pasar 3 segundos se reproduce el contenido
					$scope.playDemandStuck = 0;
					dialog.close();
					$scope.typePlayer = 'video_demand';
					$scope.video = './files/completed/' + $scope.video_demand + '?decache=' + number;
					console.log('12 >> $scope video -- cambio de video');
					var video = document.getElementById('video_player');
					$scope.video_demand_replay = $scope.video_demand_replay - 1;
					$scope.$apply();
				}, 3000);

			} else {
				//Si no existe un video on demand a reproducir  se continua normal
				//Se verifica primero que el canal no tiene asignada una url de streaming y que esta este activada
				if (typeof (config.data.channel.url) != "undefined" && config.data.channel.url.enabled == true) {
					$scope.url_streaming = config.data.channel.url.value;
				} else {
					$scope.url_streaming = '';
				}
				//Antes de comenzar con los contenidos se verifica que no exista un comunicado en cola para ser reproducido
				if ($scope.reproducirComunicado == 1) {
					//Se actualiza la lista de comunicados agregando el nuevo comunicado reproducido
					var response = ipcRenderer.sendSync('updateComunicado', $scope.idComunicado);
					if ($scope.comunicadoType == 'video') { //El comunicado es un video
						if ($scope.comunicadoText != 'SIN-TEXTO') {
							$scope.typePlayer = 'videoText';
							$scope.videoTexto = $sce.trustAsHtml($scope.comunicadoText);
						} else {
							$scope.typePlayer = 'video';
						}
						$scope.video = './files/completed/' + $scope.comunicadoValue + '?decache=' + number;
						console.log('13 >> $scope video -- cambio de video');
						var video = document.getElementById('video_player');
					} else if ($scope.comunicadoType == 'streaming') { //El comunicado es streaming
						$scope.typePlayer = 'streaming';
						$scope.video = $sce.trustAsResourceUrl($scope.comunicadoValue);
						console.log('14 >> $scope video -- cambio de video');
						//Al ser streaming se debe asignar una duracion al comunicado
						setTimeout(function() {
							$scope.$apply(function() {
								console.log('10');
								$scope.test();
							})
						}, $scope.comunicadoDuration * 1000);
					} else if ($scope.comunicadoType == 'image') { //El comunicado es una imagen
						if ($scope.comunicadoText != 'SIN-TEXTO') {
							$scope.typePlayer = 'imageText';
							$scope.imageTexto = $scope.comunicadoText;
						} else {
							$scope.typePlayer = 'image';
						}
						$scope.video = './files/completed/' + $scope.comunicadoValue + '?decache=' + number;
						console.log('15 >> $scope video -- cambio de video');
						//Al ser imagen se debe asignar una duracion al comunicado
						setTimeout(function() {
							console.log('11');
							$scope.test();
						}, $scope.comunicadoDuration * 1000);
					} else if ($scope.comunicadoType == 'text') { //El comunicado es un texto
						$scope.typePlayer = 'onlyText';
						$scope.textJustText = $sce.trustAsHtml($scope.comunicadoValue);
						$scope.video = '';
						console.log('16 >> $scope video -- cambio de video');
						//Al ser texto se debe asignar una duracion al comunicado
						setTimeout(function() {
							console.log('12');
							$scope.test();
						}, $scope.comunicadoDuration * 1000);
					}

					$scope.actualizarComunicados(); //Se actualiza el archivo de comunicados
				} else {
					//No hay videos on demand o comunicados, se continua con la programacion regular
					if (config.data.channel.blocks.length > 0) { //Verificar si existen bloques asignados

						if (config.data.channel.blocks[content.block].media[content.media].type == 'video' && $scope.url_streaming == '') { //El contenido es un video y no hay url de streaming
							var ext = config.data.channel.blocks[content.block].media[content.media].value.split('.').pop();
							if (ext != 'MP4' && ext != 'mp4') { //Revisar que el contenido es mp4
								$scope.test();
							}
							if (config.data.channel.blocks[content.block].media[content.media].text != 'SIN-TEXTO') {
								$scope.typePlayer = 'videoText';
								$scope.videoTexto = $sce.trustAsHtml(config.data.channel.blocks[content.block].media[content.media].text);
							} else {
								$scope.typePlayer = 'video';
							}

							//Se verifica de donde viene el contenido del nuevo o viejo servidor
							if ((typeof (config.data.channel.blocks[content.block].media[content.media].value_new) != undefined && config.data.channel.blocks[content.block].media[content.media].value_new != null) && (config.data.channel.blocks[content.block].media[content.media].value_new != '0')) {
								$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].value.split('/')[7] + '?decache=' + number;
								console.log('17 >> $scope video -- cambio de video');
							} else {
								$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].id + '-' + config.data.channel.blocks[content.block].media[content.media].value.split('/')[3] + '?decache=' + number;
								console.log('18 >> $scope video -- cambio de video: ');
								console.log('./files/completed/');
								console.log('|||||||||config.data.channel.blocks[content.block].media[content.media].id|||||||||');
								console.log(config.data.channel.blocks[content.block].media[content.media].id);
								console.log('|||||||||config.data.channel.blocks[content.block].media[content.media].value.split(\'/\')|||||||||');
								console.log(config.data.channel.blocks[content.block].media[content.media].value.split('/'));

							}
							var video = document.getElementById('video_player');
						} else if (config.data.channel.blocks[content.block].media[content.media].type == 'streaming' || $scope.url_streaming != '') { //El contenido es streaming
							$scope.typePlayer = 'streaming';
							if ($scope.url_streaming != '') {
								$scope.video = $sce.trustAsResourceUrl($scope.url_streaming);
								console.log('19 >> $scope video -- cambio de video');
								setTimeout(function() {
									$scope.test();
								}, 30000);
							} else {
								$scope.video = $sce.trustAsResourceUrl(config.data.channel.blocks[content.block].media[content.media].value);
								console.log('20 >> $scope video -- cambio de video');
								//Se asigna un timeout ya que el contenido es un streaming
								setTimeout(function() {
									$scope.test();
								}, config.data.channel.blocks[content.block].media[content.media].duration * 1000);
							}
						} else if (config.data.channel.blocks[content.block].media[content.media].type == 'image' && $scope.url_streaming == '') { //El contenido es una imagen y no hay url de streaming
							if (config.data.channel.blocks[content.block].media[content.media].text != 'SIN-TEXTO') {
								$scope.typePlayer = 'imageText';
								$scope.imageTexto = config.data.channel.blocks[content.block].media[content.media].text;
							} else {
								$scope.typePlayer = 'image';
							}
							//Se verifica de donde viene el contenido del nuevo o viejo servidor
							if ((typeof (config.data.channel.blocks[content.block].media[content.media].value_new) != undefined && config.data.channel.blocks[content.block].media[content.media].value_new != null) && (config.data.channel.blocks[content.block].media[content.media].value_new != '0')) {
								$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].value.split('/')[7] + '?decache=' + number;
								console.log('21 >> $scope video -- cambio de video');
							} else {
								$scope.video = './files/completed/' + config.data.channel.blocks[content.block].media[content.media].id + '-' + config.data.channel.blocks[content.block].media[content.media].value.split('/')[3] + '?decache=' + number;
								console.log('22 >> $scope video -- cambio de video');
							}
							//Se asigna un timeout ya que el contenido es una imagen
							setTimeout(function() {
								$scope.test();
							}, config.data.channel.blocks[content.block].media[content.media].duration * 1000);
						} else if (config.data.channel.blocks[content.block].media[content.media].type == 'text' && $scope.url_streaming == '') { //El contenido es un texto y no hay url de streaming
							$scope.typePlayer = 'onlyText';
							$scope.textJustText = $sce.trustAsHtml(config.data.channel.blocks[content.block].media[content.media].value);
							$scope.video = '';
							console.log('23 >> $scope video -- cambio de video');
							//Se asigna un timeout ya que el contenido es un texto
							setTimeout(function() {
								console.log('17');
								$scope.test();
							}, config.data.channel.blocks[content.block].media[content.media].duration * 1000);
						}
					} else {
						//Si no existen bloques asignados pero el canal tiene un streaming asignado
						if ($scope.url_streaming != '') {
							$scope.typePlayer = 'streaming';
							$scope.video = $sce.trustAsResourceUrl($scope.url_streaming);
							console.log('24 >> $scope video -- cambio de video');
							setTimeout(function() {
								$scope.test();
							}, 30000);
						} else {
							$scope.typePlayer = '';
							$scope.video = '';
							console.log('25 >> $scope video -- cambio de video');
						}
					}

				}
			}
		});
	}

	//Se verifica nuevamente si todavia existen comunicados por reproducir
	$scope.actualizarComunicados = function() {
		$http.get('../comunicados.json')
			.then(function(comunicados) {
				$scope.reproducirComunicado = 0;
				$scope.comunicadoValue = '';
				$scope.comunicadoType = '';
				comunicados.data.comunicados.forEach(function(comunicado, index) {
					if (comunicado.reproducido == 'no' && $scope.reproducirComunicado == 0) {
						$scope.reproducirComunicado = 1;
						$scope.idComunicado = comunicado.id;
						$scope.comunicadoValue = 'comunicado_' + comunicado.id + '_' + comunicado.value.split('/').pop();
						$scope.comunicadoType = comunicado.type;
						if ($scope.comunicadoType == 'image' || $scope.comunicadoType == 'text' || $scope.comunicadoType == 'streaming') {
							$scope.comunicadoDuration = comunicado.duration;
						}
						if ($scope.comunicadoType == 'text' || $scope.comunicadoType == 'streaming') {
							$scope.comunicadoValue = comunicado.value;
						}
						if (comunicado.text != 'SIN-TEXTO') {
							$scope.comunicadoText = comunicado.text;
						} else {
							$scope.comunicadoText = 'SIN-TEXTO';
						}
					}
				})
			});
	}

	//Esta funcion es la que asigna las imagenes laterales e inferiores y asigna los tamaños
	$scope.toogleBanner = function() {
		console.log('Activar banners');
		var screen = ipcRenderer.sendSync('screen', '1'); //Se recupera la resolucion de pantalla desde el back
		if ($scope.showClass == 2 || $scope.showClass == 0) { //se verifica si ya esta activado la imagen lateral o inferior
			var image = new Image(); //Se crea un nuevo objeto de imagen para obtener la resolucion de la misma
			image.src = $scope.imagenLateral;
			var width;
			var height;
			image.onload = function() {
				width = this.width; //Se obtiene el ancho
				height = this.height; //Se obtiene la altura

				if(width > window.innerWidth) {
					width = window.innerWidth;
				}

				var videoW = window.innerWidth - parseInt(width); //Se asigna el ancho al contenido menos el ancho de la imagen lateral
				$scope.videoWidthNew = videoW + 'px';

				//Se cambian las reglas de css3 para la animacion de aparicion, se debe tener mucho cuidado al modificar los estilos
				var lastStyleSheet = document.styleSheets[1];
				var lastStyleSheet = document.styleSheets[1].cssRules[5];
				lastStyleSheet.deleteRule("0%");
				lastStyleSheet.deleteRule("100%");
				lastStyleSheet.appendRule("0% { width:0px;}");
				lastStyleSheet.appendRule("100% { width:" + parseInt(width) + "px;}");

				//Se cambian las reglas de css3 para la animacion de aparicion, se debe tener mucho cuidado al modificar los estilos
				var lastStyleSheet = document.styleSheets[1].cssRules[9];
				lastStyleSheet.deleteRule("0%");
				lastStyleSheet.deleteRule("100%");
				lastStyleSheet.appendRule("100% { width:0px;}");
				lastStyleSheet.appendRule("0% { width:" + parseInt(width) + "px;}");

				var imageInferior = new Image(); //Se crea el objeto para la imagen inferior
				imageInferior.src = $scope.imagenInferior;
				if ($scope.imagenInferior == '' || typeof ($scope.imagenInferior) == 'undefined') {
					imageInferior.src = './files/completed/bannerInferiorModelo.png';
				}
				var widthInf;
				var heightInf;
				imageInferior.onload = function() {
					widthInf = this.width; //Se obtiene el ancho
					heightInf = this.height; //Se obtiene la altura

					var videoH = window.innerHeight - parseInt(heightInf); //Se asigna el alto al contenido menos el alto de la imagen inferior
					console.log('VIDEO HEIGHT: ', videoH);
					$scope.videoHeightNew = videoH + 'px';
					$scope.videoheight = videoH + 'px';

					//Se cambian las reglas de css3 para la animacion de aparicion, se debe tener mucho cuidado al modificar los estilos
					var lastStyleSheet = document.styleSheets[1].cssRules[3];
					lastStyleSheet.deleteRule("0%");
					lastStyleSheet.deleteRule("100%");
					lastStyleSheet.appendRule("0% { width:" + screen.width + "px; height:" + window.innerHeight + "px}");
					lastStyleSheet.appendRule("100% { width:" + videoW + "px; height:" + videoH + "px;}");

					//Se cambian las reglas de css3 para la animacion de aparicion, se debe tener mucho cuidado al modificar los estilos
					var lastStyleSheet = document.styleSheets[1].cssRules[13];
					lastStyleSheet.deleteRule("0%");
					lastStyleSheet.deleteRule("100%");
					lastStyleSheet.appendRule("100% { width:" + screen.width + "px; height:" + window.innerHeight + "px}");
					lastStyleSheet.appendRule("0% { width:" + videoW + "px; height:" + videoH + "px;}");

					//Se cambian las reglas de css3 para la animacion de aparicion, se debe tener mucho cuidado al modificar los estilos
					var lastStyleSheet = document.styleSheets[1].cssRules[7];
					lastStyleSheet.deleteRule("0%");
					lastStyleSheet.deleteRule("100%");
					lastStyleSheet.appendRule("100% { bottom:0px;}");
					lastStyleSheet.appendRule("0% { bottom:-" + parseInt(heightInf) + "px;}");

					//Se cambian las reglas de css3 para la animacion de aparicion, se debe tener mucho cuidado al modificar los estilos
					var lastStyleSheet = document.styleSheets[1].cssRules[11];
					lastStyleSheet.deleteRule("0%");
					lastStyleSheet.deleteRule("100%");
					lastStyleSheet.appendRule("0% { bottom:0px;}");
					lastStyleSheet.appendRule("100% { bottom:-" + parseInt(heightInf) + "px;}");

					//Se asigna el tamaño de la imagen inferior para que ocupe el espacio completo
					var inferiorwidth = window.innerWidth - width;
					$scope.inferiorwidth = inferiorwidth + 'px';
					$scope.inferiorheight = heightInf + 'px';

					//Se asigna el tamaño de la imagen lateral para que ocupe el espacio completo
					var lateralheight = window.innerHeight;

					$scope.lateralwidth = width + 'px';
					$scope.lateralheight = window.innerHeight + 'px';

					console.log('lateralWidth', $scope.lateralwidth);
					console.log('lateralheight', $scope.lateralheight);

					$scope.showClass = 1
				};
			};

		} else {
			$scope.videoWidth = window.innerWidth + 'px';
			$scope.videoheight = window.innerHeight + 'px';
			$scope.showClass = 0;
		}

	}

	//Funcion para actualizar las medidas de las imagenes laterales e inferiores siguientes, con esto se pueden cargar imagenes de diferentes resoluciones
	$scope.updateSpecs = function() {
		var screen = ipcRenderer.sendSync('screen', '1');
		var image = new Image();
		image.src = $scope.imagenLateral;
		var width;
		var height;
		image.onload = function() {
			width = this.width;
			height = this.height;


			if(width > window.innerWidth) {
				width = window.innerWidth;
			}

			// insertarrr

			var videoW = window.innerWidth - parseInt(width);
			$scope.videoWidthNew = videoW + 'px';
			var lastStyleSheet = document.styleSheets[1];

			var lastStyleSheet = document.styleSheets[1].cssRules[5];
			lastStyleSheet.deleteRule("0%");
			lastStyleSheet.deleteRule("100%");
			lastStyleSheet.appendRule("0% { width:0px;}");
			lastStyleSheet.appendRule("100% { width:" + parseInt(width) + "px;}");

			var lastStyleSheet = document.styleSheets[1].cssRules[9];
			lastStyleSheet.deleteRule("0%");
			lastStyleSheet.deleteRule("100%");
			lastStyleSheet.appendRule("100% { width:0px;}");
			lastStyleSheet.appendRule("0% { width:" + parseInt(width) + "px;}");

			var imageInferior = new Image();
			imageInferior.src = $scope.imagenInferior;
			if ($scope.imagenInferior == '' || typeof ($scope.imagenInferior) == 'undefined') {
				imageInferior.src = './files/completed/bannerInferiorModelo.png';
			}
			var widthInf;
			var heightInf;
			imageInferior.onload = function() {
				widthInf = this.width;
				heightInf = this.height;
				var videoH = window.innerHeight - parseInt(heightInf);
				console.log('VIDEO HEIGHT', videoH);
				$scope.videoHeightNew = videoH + 'px';

				var lastStyleSheet = document.styleSheets[1].cssRules[3];
				lastStyleSheet.deleteRule("0%");
				lastStyleSheet.deleteRule("100%");
				lastStyleSheet.appendRule("0% { width:" + screen.width + "px; height:" + window.innerHeight + "px}");
				lastStyleSheet.appendRule("100% { width:" + videoW + "px; height:" + videoH + "px;}");

				var lastStyleSheet = document.styleSheets[1].cssRules[13];
				lastStyleSheet.deleteRule("0%");
				lastStyleSheet.deleteRule("100%");
				lastStyleSheet.appendRule("100% { width:" + screen.width + "px; height:" + window.innerHeight + "px}");
				lastStyleSheet.appendRule("0% { width:" + videoW + "px; height:" + videoH + "px;}");

				var lastStyleSheet = document.styleSheets[1].cssRules[7];
				lastStyleSheet.deleteRule("0%");
				lastStyleSheet.deleteRule("100%");
				lastStyleSheet.appendRule("100% { bottom:0px;}");
				lastStyleSheet.appendRule("0% { bottom:-" + parseInt(heightInf) + "px;}");

				var lastStyleSheet = document.styleSheets[1].cssRules[11];
				lastStyleSheet.deleteRule("0%");
				lastStyleSheet.deleteRule("100%");
				lastStyleSheet.appendRule("0% { bottom:0px;}");
				lastStyleSheet.appendRule("100% { bottom:-" + parseInt(heightInf) + "px;}");


				var inferiorwidth = window.innerWidth - width;
				$scope.inferiorwidth = inferiorwidth + 'px';
				$scope.inferiorheight = heightInf + 'px';





				var lateralheight = window.innerHeight;
				$scope.lateralwidth = width + 'px';
				$scope.lateralheight = window.innerHeight + 'px';
				//$scope.lateralheight = lateralheight + 'px';

			};
		};
	}

	//Esta funcion sirve para mostrar las imagenes laterales o inferiores las activa por la duracion que sea enviada
	$scope.toogleBannerApply = function(time, timeNext) {

		var time = parseInt($scope.config.channel.lateral.duration) * 1000; //Duracion de la imagen en pantalla
		var timeNext = parseInt($scope.config.channel.lateral.interval) * 1000; //Intervalo para mostrar nuevamente la imagen

		$scope.showClass = 1;

		if (!$scope.config.channel.lateral.overlay) {
			$scope.videoWidth = $scope.videoWidthNew;
			$scope.videoHeight = $scope.videoHeightNew;
			$scope.overlay = false;
		} else {
			$scope.overlay = true;
		}
		console.log('overlay', $scope.overlay);
		$scope.$apply();
		$scope.intervalApplyHide = setTimeout(function() {
			$scope.toogleBannerApplyHide(time, timeNext);
		}, time); //Al terminar la duracion de la imagen ejecuta la funcion para ocultarlas
	}

	//Esta funcion sirve para ocultar las iamgenes laterales o inferiores por el intervalo que sea enviado
	$scope.toogleBannerApplyHide = function(time, timeNext) {
		var screen = ipcRenderer.sendSync('screen', '1');
		var time = parseInt($scope.config.channel.lateral.duration) * 1000; //Duracion de la imagen en pantalla
		var timeNext = parseInt($scope.config.channel.lateral.interval) * 1000; //Intervalo para mostrar nuevamente la imagen

		$scope.videoWidth = screen.width + 'px';
		$scope.videoHeight = window.innerHeight + 'px';

		console.log('video width: ', $scope.videoWidth);
		console.log('video height: ', $scope.videoHeight);

		$scope.showClass = 0;
		$scope.$apply();

		var number = Math.floor((Math.random() * 100) + 1); //Numero al azar para evitar que no se cambien las imagenes

		setTimeout(function() {
			//Se actualiza nueva imagen inferior y se envia al back la nueva imagen actual
			$http.get('../inferior.json').then(function(inferior) {
				var actual = ++inferior.data.actual;
				if (typeof ($scope.config.channel.imagenesInferiores[actual]) == "undefined") {
					var content = {
						"actual": 0
					};
					actual = 0;
				} else {
					var content = {
						"actual": actual
					};
				}
				var iInferiores = ipcRenderer.sendSync('imagenesInferiores', angular.toJson(content));
				if ($scope.config.channel.imagenesInferiores.length != 0) {
					//Se verifica si la imagen esta en el nuevo o viejo servidor
					if ($scope.config.channel.imagenesInferiores[actual].picture_new == -1) {
						$scope.imagenInferior = './files/completed/bottom_image_' + $scope.config.channel.imagenesInferiores[actual].picture.split('/')[3] + '?decache=' + number;
					} else {
						$scope.imagenInferior = './files/completed/bottom_image_' + $scope.config.channel.imagenesInferiores[actual].picture.split('/')[3] + '?decache=' + number;
					}

				} else {
					$scope.imagenInferior = '';
				}

				$scope.updateSpecs(); //Se llama a funcion para actualizar las dimensiones del contenido e imagenes
				$scope.videoWidth = screen.width + 'px';
			});

			//Se actualiza nueva imagen lateral y se envia al back la nueva imagen actual
			$http.get('../lateral.json').then(function(lateral) {
				var actual = ++lateral.data.actual;
				if (typeof ($scope.config.channel.imagenesLaterales[actual]) == "undefined") {
					var content = {
						"actual": 0
					};
					actual = 0;
				} else {
					var content = {
						"actual": actual
					};
				}

				var iLaterales = ipcRenderer.sendSync('imagenesLaterales', angular.toJson(content));

				//Se verifica si la imagen esta en el nuevo o viejo servidor
				if ($scope.config.channel.imagenesLaterales[actual].picture_new == -1) {
					$scope.imagenLateral = './files/completed/lateral_image_' + $scope.config.channel.imagenesLaterales[actual].picture.split('/')[3] + '?decache=' + number;
				} else {
					$scope.imagenLateral = './files/completed/lateral_image_' + $scope.config.channel.imagenesLaterales[actual].picture.split('/')[3] + '?decache=' + number;
				}

				$scope.updateSpecs(); //Se llama a funcion para actualizar las dimensiones del contenido e imagenes
				$scope.videoWidth = screen.width + 'px';

			});

			$scope.intervalApply = setTimeout(function() {
				$scope.toogleBannerApply(time, timeNext);
			}, timeNext); //Se vuleve a ejecutar funcion para mostrar nuevamente las imagenes
		}, 4000);

	}

	//Funcion para ocultar las imagenes laterales e inferiores e indicar que estas han sifo deshabilitadas
	$scope.toogleBannerHide = function() {
		var screen = ipcRenderer.sendSync('screen', '1');
		$scope.videoWidth = screen.width + 'px';
		$scope.videoHeight = window.innerHeight + 'px';
		$scope.showClass = 2;
	}

	//Funcion que captura la primera tecla presionada para ser comparada con la siguiente
	$scope.doSomething = function($event) {
		$scope.keyDown = $event.keyCode;
	}

	//Funcion que captura la siguiente tecla presionada para compararla con la anterior y ver si es un comando especial
	$scope.doSomethingUp = function($event) {
		$scope.keyUp = $event.keyCode;
		if (($scope.keyDown == '17' && $scope.keyUp == '48') || ($scope.keyDown == '48' && $scope.keyUp == '17')) { //Comando de teclas para abrir menu de especificaciones del sistema
			$scope.play_video_demand = 0;
			$scope.menuId = ngDialog.open({
				template: 'views/menu.html',
				className: 'ngdialog-theme-default',
				controller: 'winboxCtrl',
				scope: $scope
			});
			$scope.menuId.closePromise.then(function(data) {
				if (typeof (data.$dialog != 'undefined')) {
					if (data.$dialog.scope().play_video_demand == '1') {
						$scope.video_demand = data.$dialog.scope().video_demand;
						$scope.video_demand_replay = data.$dialog.scope().video_demand_replay;
						$scope.test();
					}
				}
			});
		}

		if (($scope.keyDown == '17' && $scope.keyUp == '57') || ($scope.keyDown == '57' && $scope.keyUp == '17')) { //Comando de teclas para abrir menu de comandos especiales
			ngDialog.open({
				template: 'views/hidden_menu.html',
				className: 'ngdialog-theme-default',
				controller: 'winboxCtrl',
				scope: $scope
			});
		}
	}

	//Funcion para cambiar de menu en el menu de datos del sistema
	$scope.tab = function(action) {
		if (action == 'home') {
			$scope.hometab = true;
			$scope.menutab1 = false;
			$scope.menutab2 = false;
		} else if (action == 'menu1') {
			$scope.hometab = false;
			$scope.menutab1 = true;
			$scope.menutab2 = false;
		} else {
			$scope.hometab = false;
			$scope.menutab1 = false;
			$scope.menutab2 = true;
		}
	}

	//funcion que ordena los blocks
	$scope.sortBlocks = function(blocks) {
		var listaReturn = blocks;
		listaReturn = listaReturn.sort(function(a, b) {
			return (a.id - b.id)
		});

		return listaReturn;
	}

	//funcion que ordena los blocks
	$scope.sortBanners = function(banners) {
		var listaReturn = banners;
		listaReturn = listaReturn.sort(function(a, b) {
			return (a.id - b.id)
		});

		return listaReturn;
	}

	//funcion que da el proximo block con media
	$scope.siguienteBloqueValido = function(bloques, bloqueActual) {
		if (bloqueActual >= bloques.length - 1) {
			var it = 0;
		} else {
			var it = bloqueActual + 1;
		}
		var content = -1;
		for (var i = it; i != bloqueActual; i++) {
			//primer block encontrado que tiene media, asi que lo asignamos
			if (bloques[i].media.length > 0) {
				var content = { //Se asigna siguiente contenido
					block: i,
					media: 0
				};
				break;
			}

			if (i == bloques.length - 1) {
				i = -1;
			}
		}
		if (content == -1) {
			var content = { //Se asigna siguiente contenido
				block: 0,
				media: 0
			};
		} else { }

		return content;
	}

}]);
